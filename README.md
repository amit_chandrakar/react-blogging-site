<div class="markdown prose w-full break-words dark:prose-invert dark">
    <h1>React Blogging App</h1>
    <p>A React Blogging App is a web application built using the popular JavaScript library, React.js. This app allows users to create, read, update, and delete blog posts. The user interface is clean and easy to navigate, making it simple for users to interact with the app.</p>
    <p>The app uses React's component-based architecture, which allows for easy modularization of code and efficient updates to the user interface. The app also utilizes the latest web development tools such as webpack and babel, making it fast and responsive.</p>
    <p>Users can create new blog posts by navigating to the 'Create Post' page and filling out a simple form. The form includes fields for the post title, content, and an optional image. Once the user submits the form, the new post will be added to the list of all posts and will be visible to all other users.</p>
    <p>Users can also read and update existing posts by clicking on the title of a post. The post's content will be displayed and users can edit the post by clicking on the 'Edit' button. The changes made will be saved automatically and will be reflected on the list of all posts.</p>
    <p>The app also allows users to delete existing posts. Users can simply click on the 'Delete' button on a post and the post will be removed from the list of all posts.</p>
    <p>Overall, the React Blogging App is a user-friendly and efficient way for users to create, read, update, and delete blog posts. The app's use of React.js makes it fast and responsive, while the component-based architecture allows for easy modularization of code.</p>
    <h2>Getting Started</h2>
    <p>These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.</p>
    <h3>Prerequisites</h3>
    <ul>
        <li><a href="https://nodejs.org/" target="_new">Node.js</a></li>
        <li><a href="https://www.npmjs.com/" target="_new">npm</a></li>
    </ul>
    <h3>Installing</h3>
    <ol>
        <li>Clone the repository to your local machine</li>
    </ol>
    <pre><div class="bg-black mb-4 rounded-md"><div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans"></div><div class="p-4 overflow-y-auto"><code class="!whitespace-pre-wrap hljs language-bash">git <span class="hljs-built_in">clone</span> git@gitlab.com:amit_chandrakar/react-blogging-site.git
</code></div></div></pre>
    <ol start="2">
        <li>Navigate to the project directory</li>
    </ol>
    <pre><div class="bg-black mb-4 rounded-md"><div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans"></div><div class="p-4 overflow-y-auto"><code class="!whitespace-pre-wrap hljs language-bash"><span class="hljs-built_in">cd</span> react-blogging-app
</code></div></div></pre>
    <ol start="3">
        <li>Install the necessary packages</li>
    </ol>
    <pre><div class="bg-black mb-4 rounded-md"><div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans"></div><div class="p-4 overflow-y-auto"><code class="!whitespace-pre-wrap hljs">npm install
</code></div></div></pre>
    <p>or</p>
    <pre><div class="bg-black mb-4 rounded-md"><div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans"></div><div class="p-4 overflow-y-auto"><code class="!whitespace-pre-wrap hljs">yarn install
</code></div></div></pre>
    <h3>Import data in Firebase</h3>
    <ol>
        <li>Login to your firebase account and click on <a href="https://console.firebase.google.com/" target="_new">Go to console</a></li>
    </ol>
    <ol start="2">
        <li>Create new "Realtime Database"</li>
    </ol>
    <ol start="3">
        <li>Import JSON file given in file location - <span class="hljs-built_in">PROJECT_ROOT/Firebase/blog.json</span></li>
    </ol>
    <ol start="4">
        <li>Rename .env.example file to .env and provide your database url. For example - <a href="#" target="_new">https://blog-XXXXX-default-XXXX.firebaseio.com</a></li>
    </ol><h3>Running the app</h3>
    <ol>
        <li>Start the development server</li>
    </ol>
    <pre><div class="bg-black mb-4 rounded-md"><div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans"></div><div class="p-4 overflow-y-auto"><code class="!whitespace-pre-wrap hljs language-sql">npm <span class="hljs-keyword">start</span>
</code></div></div></pre>
    <p>or</p>
    <pre><div class="bg-black mb-4 rounded-md"><div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans"></div><div class="p-4 overflow-y-auto"><code class="!whitespace-pre-wrap hljs language-sql">yarn <span class="hljs-keyword">start</span>
</code></div></div></pre><ol start="2">
        <li>Open <a href="http://localhost:3000" target="_new">http://localhost:3000</a> to view the app in the browser.</li>
    </ol>
    <h2>Built With</h2>
    <ul>
        <li><a href="https://reactjs.org/" target="_new">React</a> - A JavaScript library for building user interfaces</li>
        <li><a href="https://nodejs.org/" target="_new">Firebase</a> - Set of hosting services. </li>
    </ul>
    <h2>Dependencies</h2>
    <p>Apart from the all default dependencies which comes with react installation, application needs following dependencies to be installed -</p>
    <ul>
        <li><a href="https://reactrouter.com/en/main" target="_new">react-router-dom</a></li>
        <li><a href="https://www.npmjs.com/package/@mui/material" target="_new">mui/material</a></li>
        <li><a href="https://react-data-table-component.netlify.app/?path=/story/getting-started-intro--page" target="_new">react-data-table-component</a></li>
        <li><a href="https://ckeditor.com/docs/ckeditor5/latest/installation/index.html" target="_new">ckeditor5-react</a></li>
        <li><a href="https://www.npmjs.com/package/@mui/x-date-pickers" target="_new">mui/x-date-pickers</a></li>
        <li><a href="https://www.npmjs.com/package/axios" target="_new">axios</a></li>
        <li><a href="https://www.npmjs.com/package/moment" target="_new">moment</a></li>
        <li><a href="https://styled-components.com/" target="_new">styled-components</a></li>
        <li><a href="https://sweetalert2.github.io/" target="_new">sweetalert2</a></li>
        <li><a href="https://github.com/sweetalert2/sweetalert2-react-content" target="_new">sweetalert2-react-content</a></li>
        <li><a href="https://formik.org/" target="_new">formik</a></li>
        <li><a href="https://github.com/jquense/yup" target="_new">yup</a></li>
    </ul>
    <h2>Author</h2>
    <ul>
        <li><strong>Amit Kumar Chandrakar (Senior Full Stack Developer)</strong> - <a href="javascript:;" target="_new">amitchandrakar.com</a></li>
    </ul>
    <h2>License</h2>
    <p>This project is licensed under the <a href="https://opensource.org/licenses/MIT" target="_new">MIT License</a> - see the <a href="LICENSE.md" target="_new">LICENSE.md</a> file for details.</p>
    <h2>Sponsor 💰</h2>
    <p>I fell in love with open-source in 2017 and there has been no looking back since! You can read more about me</p>
    <p>☕ How about we get to know each other over coffee? Buy me a cup for just $5</p>
    <h2>Credits</h2>
    <ul>
    <li><a href="https://openai.com/blog/chatgpt/" target="_new">ChatGPT</a> - For content writing. </li>
    </ul>
    <h2>Features to implement</h2>
    <li>Understand folder structure</li>
<li>Understand the flow of the code</li>
<li>CRUD operations</li>
<li>Form Repeater</li>
<li>Pagination</li>
<li>Necessary plugins like</li>
    <ul>
        <li>Datatable</li>
        <li>Select2</li>
        <li>Toastr</li>
        <li>Datepicker</li>
        <li>Colorpicker</li>
        <li>Timepicker</li>
        <li>Sweetalert</li>
        <li>Dropzone</li>
        <li>Dropify</li>
        <li>Summernote</li>
        <li>Validation</li>
        <li>Cropper</li>
        <li>moment.js</li>
        <li>Chartjs</li>
        <li>Fullcalendar</li>
    </ul>
<li>File/Image upload</li>
<li>AWS S3 integration</li>
<li>Admin Dashboard</li>
<li>Authentication</li>
    <ul>
        <li>Login</li>
        <li>Register</li>
        <li>Forgot password</li>
        <li>Reset password</li>
        <li>Email verification</li>
        <li>Social login</li>
    </ul>
<li>Customization and Themes (Allow users to customize the app appearance, such as themes, fonts, </li>and colors)
<li>Locale</li>
<li>Payment gateway</li>
    <ul>
        <li>Stripe</li>
        <li>Paypal</li>
        <li>Razorpay</li>
    </ul>
<li>SMS Gateway</li>
<li>Google recaptcha</li>
<li>Notifications</li>
    <ul>
        <li>Push notification</li>
        <li>Email notification</li>
        <li>Slack notification</li>
    </ul>
<li>Seeder</li>
<li>Chat</li>
<li>Google Calendar</li>
<li>Google maps</li>
<li>Database backup</li>
<li>Zoom integration</li>
<li>User Activity Logs</li>
<li>User Roles and Permissions</li>
<li>Integrations</li>
    <ul>
        <li>Docker</li>
        <li>CI/CD</li>
        <li>Linting and Code Formatting</li>
        <li>Code Documentation</li>
    </ul>
</div>
