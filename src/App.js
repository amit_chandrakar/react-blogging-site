import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

import ProtectedRoute from "./components/ProtectedRoute";

import SignIn from "./pages/auth/SignIn";
import SignUp from "./pages/auth/SignUp";

import Dashboard from "./pages/dashboard/Dashboard";
import ErrorPage from "./pages/error/ErrorPage";
import Setting from "./pages/settings/Setting";
import Profile from "./pages/profile/Profile";

import {Index as Category} from "./pages/category/Index";
import { Create as CreateCategory } from "./pages/category/Create";
import { Edit as EditCategory } from "./pages/category/Edit";
import { Show as ShowCategory } from "./pages/category/Show";

import { Index as SubCategory} from "./pages/sub-category/Index";
import { Create as CreateSubCategory } from "./pages/sub-category/Create";
import { Edit as EditSubCategory } from "./pages/sub-category/Edit";
import { Show as ShowSubCategory } from "./pages/sub-category/Show";

import { Index as Posts } from "./pages/post/Index";
import { Create as CreatePost } from "./pages/post/Create";
import { Edit as EditPost } from "./pages/post/Edit";
import { Show as ShowPost } from "./pages/post/Show";

import Home from "./pages/front-website/Home";
import Post from "./pages/front-website/Post";
import Contact from "./pages/front-website/Contact";
import BuyMeCoffee from "./pages/front-website/BuyMeCoffee";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
    errorElement: <ErrorPage />,
  },
  {
    path: "/post/:slug",
    element: <Post />,
    errorElement: <ErrorPage />,
  },
  {
    path: "/contact",
    element: <Contact />,
    errorElement: <ErrorPage />,
  },
  {
    path: "/buy-me-a-coffee",
    element: <BuyMeCoffee />,
    errorElement: <ErrorPage />,
  },
  {
    path: "/signin",
    element: <SignIn />,
    errorElement: <ErrorPage />,
  },
  {
    path: "/signup",
    element: <SignUp />,
    errorElement: <ErrorPage />,
  },
  {
    path: "/dashboard",
    element:
    <>
      <ProtectedRoute>
        <Dashboard />
      </ProtectedRoute>
    </>
    ,
    errorElement: <ErrorPage />,
  },
  {
    path: "/category",
    element:
    <>
      <ProtectedRoute>
        <Category />
      </ProtectedRoute>
    </>
    ,
    errorElement: <ErrorPage />,
  },
  {
    path: "/category/create",
    element:
    <>
      <ProtectedRoute>
        <CreateCategory />
      </ProtectedRoute>
    </>
    ,
    errorElement: <ErrorPage />,
  },
  {
    path: "/category/edit/:id",
    element:
    <>
      <ProtectedRoute>
        <EditCategory />
      </ProtectedRoute>
    </>
    ,
    errorElement: <ErrorPage />,
  },
  {
    path: "/category/show/:id",
    element:
    <>
      <ProtectedRoute>
        <ShowCategory />
      </ProtectedRoute>
    </>
    ,
    errorElement: <ErrorPage />,
  },
  {
    path: "/sub-category/create",
    element:
    <>
      <ProtectedRoute>
        <CreateSubCategory />
      </ProtectedRoute>
    </>
    ,
    errorElement: <ErrorPage />,
  },
  {
    path: "/sub-category/edit/:id",
    element:
    <>
      <ProtectedRoute>
        <EditSubCategory />
      </ProtectedRoute>
    </>
    ,
    errorElement: <ErrorPage />,
  },
  {
    path: "/sub-category/show/:id",
    element:
    <>
      <ProtectedRoute>
        <ShowSubCategory />
      </ProtectedRoute>
    </>
    ,
    errorElement: <ErrorPage />,
  },
  {
    path: "/sub-category",
    element:
    <>
      <ProtectedRoute>
        <SubCategory />
      </ProtectedRoute>
    </>
    ,
    errorElement: <ErrorPage />,
  },
  {
    path: "/post",
    element:
    <>
      <ProtectedRoute>
        <Posts />
      </ProtectedRoute>
    </>
    ,
    errorElement: <ErrorPage />,
  },
  {
    path: "/post/create",
    element:
    <>
      <ProtectedRoute>
        <CreatePost />
      </ProtectedRoute>
    </>
    ,
    errorElement: <ErrorPage />,
  },
  {
    path: "/post/edit/:id",
    element:
    <>
      <ProtectedRoute>
        <EditPost />
      </ProtectedRoute>
    </>
    ,
    errorElement: <ErrorPage />,
  },
  {
    path: "/post/show/:id",
    element:
    <>
      <ProtectedRoute>
        <ShowPost />
      </ProtectedRoute>
    </>
    ,
    errorElement: <ErrorPage />,
  },
  {
    path: "/settings",
    element:
    <>
      <ProtectedRoute>
        <Setting />
      </ProtectedRoute>
    </>
    ,
    errorElement: <ErrorPage />,
  },
  {
    path: "/profile",
    element:
    <>
      <ProtectedRoute>
        <Profile />
      </ProtectedRoute>
    </>
    ,
    errorElement: <ErrorPage />,
  },
  {
    path: "*",
    element: <ErrorPage />,
  }
]);


function App()
{
  return (
    <>
      <RouterProvider router={router} />
    </>
  );
}

export default App;
