import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import { Link as ReactRouterLink } from "react-router-dom";

export default function Copyright(props) {
    return (
        <Typography variant="body2" color="text.secondary" align="center" {...props}>
            {'Copyright © '}
            <Link component={ReactRouterLink} to="/">
                Home Page
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}