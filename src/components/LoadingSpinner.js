import React from "react";
import "./spinner.css";

export default function LoadingSpinner(props) 
{

    let message = props.message ? props.message : `Loading...`;

    return (
        <div className="spinner-container">
            <div className="loading-spinner">
            </div>
            <p>
                {message}
            </p>
        </div>
    );
}