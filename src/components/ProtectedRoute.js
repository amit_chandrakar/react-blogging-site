import React from 'react';
import { Navigate } from "react-router-dom";

export default function ProtectedRoute({ children }) 
{
    const loggedInUser = JSON.parse(localStorage.getItem("isLoggedIn"));

    if (!loggedInUser) {
        // user is not authenticated
        return <Navigate to="/signin" />;
    }

    return children;
}