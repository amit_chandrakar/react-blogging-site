import React from 'react';
import { Link as ReactRouterLink } from "react-router-dom";

function Navbar()
{
    const company = JSON.parse(localStorage.getItem('company'));

    return (
        <nav className="navbar navbar-expand-lg navbar-light" id="mainNav">
            <div className="container px-4 px-lg-5">
                <ReactRouterLink className="navbar-brand" to="/">{company ? company.app_name : 'Blogging App'}</ReactRouterLink>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i className="fas fa-bars"></i>
                </button>
                <div className="collapse navbar-collapse" id="navbarResponsive">
                    <ul className="navbar-nav ms-auto py-4 py-lg-0">
                        <li className="nav-item">
                            <ReactRouterLink className="nav-link px-lg-3 py-3 py-lg-4" to="/">Home</ReactRouterLink>
                        </li>
                        <li className="nav-item">
                            <ReactRouterLink className="nav-link px-lg-3 py-3 py-lg-4" to="/contact">Contact</ReactRouterLink>
                        </li>
                        <li className="nav-item">
                            <ReactRouterLink className="nav-link px-lg-3 py-3 py-lg-4" to="/signin">Login</ReactRouterLink>
                        </li>
                        <li className="nav-item">
                            <ReactRouterLink className="nav-link px-lg-3 py-3 py-lg-4" to="/buy-me-a-coffee">Buy me a coffee</ReactRouterLink>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default Navbar