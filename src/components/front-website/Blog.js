import React from 'react';
import { Link as ReactRouterLink } from "react-router-dom";

function Blog(props) {

  return (
    <>
        {/* START */}
          <div className="post-preview">
              <ReactRouterLink to={'/post/'+ props.post.slug}>
                  <h2 className="post-title">
                    {props.post.title}
                  </h2>
                  <h3 className="post-subtitle">
                    {props.post.sub_title}
                  </h3>
              </ReactRouterLink>
              <p className="post-meta">
                  Posted by <b>Admin</b> on September 24, 2021
              </p>
          </div>
          <hr className="my-4" />
        {/* END */}
    </>
  )
}

export default Blog