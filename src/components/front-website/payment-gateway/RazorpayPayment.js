import React, { useCallback, useState } from "react";
import useRazorpay from "react-razorpay";
import axios from "axios";

function RazorpayPayment()
{
    const [Razorpay] = useRazorpay();
    const [isLoading, setIsLoading] = useState(false);

    const createOrder = async () => {
        var response;

        try {
            response = await axios.post(
                `${process.env.REACT_APP_FIREBASE_BASE_URL}/payments/create-order`,
                {
                    headers: {
                        'Authorization': `Bearer ${localStorage.getItem("token")}`
                    }
                }
            );
        } catch (error) {
            console.log(error);
        }

        return response;
    };

    const handlePayment = useCallback(async () => {

        setIsLoading(true);

        var order = await createOrder();
        order = order.data;

        var options = {
            key: "rzp_test_qTSIU9xaKrYa3O",
            amount: order.amount,
            currency: order.currency,
            name: "Test",
            description: "Test Transaction",
            order_id: order.id,
            image: "https://img.freepik.com/free-vector/bird-colorful-gradient-design-vector_343694-2506.jpg",
            handler: (res) => {
                setIsLoading(false);
            },
            prefill: {
                name: "Amit Kumar Chandrakar",
                email: "amit@example.com",
                contact: "9999999999",
            },
            notes: {
                address: "Razorpay Corporate Office",
            },
            theme: {
                color: "#3399cc",
            },
        };

        var rzpay = new Razorpay(options);

        rzpay.on("payment.failed", function (response) {
            // console.log(response.error.code);
            // console.log(response.error.description);
            // console.log(response.error.source);
            // console.log(response.error.step);
            // console.log(response.error.reason);
            // console.log(response.error.metadata.order_id);
            // console.log(response.error.metadata.payment_id);
            setIsLoading(false);
        });

        rzpay.open();
    }, [Razorpay]);

    return (
        <div className="col-md-4 col-lg-4 col-xl-4">
            <div className="text-center mb-2 fw-bold">Razorpay</div>
            <button onClick={handlePayment} disabled={isLoading} >
                <img
                    src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png"
                    alt="Buy Me A Coffee"
                    style={{
                        height: "60px",
                        width: "217px",
                    }}
                />
            </button>
        </div>
    );
}

export default RazorpayPayment;
