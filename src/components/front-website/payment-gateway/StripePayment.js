import React, { useCallback, useState } from "react";
import axios from "axios";
// const Stripe = require('stripe');
import Stripe from 'stripe';
const stripe = new Stripe(process.env.STRIPE_SECRET);

function StripePayment()
{
    const [isLoading, setIsLoading] = useState(false);

    const handlePayment = async () => {
        try {
            const paymentIntent = await stripe.paymentIntents.create({
                amount: 1099,
                currency: 'usd',
                payment_method_types: ['card'],
              });
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <div className="col-md-4 col-lg-4 col-xl-4">
            <div className="text-center mb-2 fw-bold">Stripe</div>
            <button onClick={handlePayment} disabled={isLoading}>
                <img
                    src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png"
                    alt="Buy Me A Coffee"
                    style={{
                        height: "60px",
                        width: "217px",
                    }}
                />
            </button>
        </div>
    );
}

export default StripePayment;
