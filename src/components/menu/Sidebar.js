import React from 'react';
import { useTheme } from '@mui/material/styles';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import DashboardIcon from '@mui/icons-material/Dashboard';
import CategoryIcon from '@mui/icons-material/Category';
import AutoAwesomeMosaicIcon from '@mui/icons-material/AutoAwesomeMosaic';
import PostAddIcon from '@mui/icons-material/PostAdd';
import SettingsIcon from '@mui/icons-material/Settings';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import Avatar from '@mui/material/Avatar';
import {
    Link as ReactRouterLink,
    useLocation,
    useNavigate
} from "react-router-dom";
import {
    DrawerHeader,
    AppBar,
    Drawer
} from "./SidebarData";
import PersonIcon from '@mui/icons-material/Person';
import LogoutIcon from '@mui/icons-material/Logout';

function Sidebar(props)
{
    const theme = useTheme();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const navigate = useNavigate();
    const location = useLocation();

    // Check if user is logged in
    // const loggedInUser = JSON.parse(localStorage.getItem("user"));
    const companySettings = JSON.parse(localStorage.getItem("company"));
    const [open, setOpen] = React.useState(companySettings.miniDrawer === 'yes' ? false : true);

    // if (loggedInUser === null) {
    //     navigate('/');
    // }

    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    // Logout the user
    const handleLogout = () => {
        localStorage.clear();
        navigate('/');
    };

    return (
        <>
            <CssBaseline />

            <AppBar style={{ background: '#2E3B55' }} position="fixed" open={open}>
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        sx={{
                            marginRight: 5,
                            ...(open && { display: 'none' }),
                        }}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" noWrap component="div" sx={{ flexGrow: 1 }}>
                        {props.title}
                    </Typography>

                    {/* <Search>
                        <SearchIconWrapper>
                            <SearchIcon />
                        </SearchIconWrapper>
                        <StyledInputBase
                            placeholder="Search…"
                            inputProps={{ 'aria-label': 'search' }}
                        />
                    </Search> */}

                    <div>
                        <IconButton
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleMenu}
                            color="inherit"
                        >
                            <Avatar alt="" src="./" />
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorEl}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={Boolean(anchorEl)}
                            onClose={handleClose}
                        >

                            <MenuItem>
                                <ListItemIcon>
                                    <PersonIcon fontSize="small" />
                                </ListItemIcon>
                                <ListItemText onClick={() => navigate('/profile')}>Update Profile</ListItemText>
                            </MenuItem>
                            <MenuItem>
                                <ListItemIcon>
                                    <LogoutIcon fontSize="small" />
                                </ListItemIcon>
                                <ListItemText onClick={handleLogout}>Logout</ListItemText>
                            </MenuItem>
                        </Menu>
                    </div>

                </Toolbar>
            </AppBar>

            <Drawer
                variant="permanent"
                open={open}>
                <DrawerHeader>
                    <Typography sx={{ mx: 'auto' }}>
                        { companySettings.appName }
                    </Typography>
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                    </IconButton>
                </DrawerHeader>
                <Divider />

                {/* Start List Item */}
                <List>
                    <ListItem disablePadding sx={{ display: 'block' }} component={ReactRouterLink} to="/dashboard" selected={'/dashboard'===location.pathname} style={{ color: 'black' }}>
                        <ListItemButton>
                            <ListItemIcon
                                sx={{
                                    minWidth: 0,
                                    mr: open ? 3 : 'auto',
                                    justifyContent: 'center',
                                }}
                            >
                                <DashboardIcon />
                            </ListItemIcon>
                            <ListItemText primary="Dashboard" sx={{ opacity: open ? 1 : 0 }} />
                        </ListItemButton>
                    </ListItem>
                    <ListItem
                        disablePadding
                        sx={{ display: 'block' }}
                        component={ReactRouterLink}
                        to="/category"
                        style={{ color: 'black' }}
                        selected={
                            location.pathname.toLowerCase().includes('/category') ||
                            location.pathname.toLowerCase().includes('/category/create') ||
                            location.pathname.toLowerCase().includes('/category/edit')
                        }
                    >
                        <ListItemButton>
                            <ListItemIcon
                                sx={{
                                    minWidth: 0,
                                    mr: open ? 3 : 'auto',
                                    justifyContent: 'center',
                                }}
                            >
                                <CategoryIcon />
                            </ListItemIcon>
                            <ListItemText primary="Categories" sx={{ opacity: open ? 1 : 0 }} />
                        </ListItemButton>
                    </ListItem>

                    <ListItem
                        disablePadding
                        sx={{ display: 'block' }}
                        component={ReactRouterLink}
                        to="/sub-category"
                        selected={
                            location.pathname.toLowerCase().includes('/sub-category') ||
                            location.pathname.toLowerCase().includes('/sub-category/create') ||
                            location.pathname.toLowerCase().includes('/sub-category/edit')
                        }
                        style={{ color: 'black' }}
                    >
                        <ListItemButton>
                            <ListItemIcon
                                sx={{
                                    minWidth: 0,
                                    mr: open ? 3 : 'auto',
                                    justifyContent: 'center',
                                }}
                            >
                                <AutoAwesomeMosaicIcon />
                            </ListItemIcon>
                            <ListItemText primary="Sub-Categories" sx={{ opacity: open ? 1 : 0 }} />
                        </ListItemButton>
                    </ListItem>
                    <ListItem
                        disablePadding
                        sx={{ display: 'block' }}
                        component={ReactRouterLink}
                        to="/post"
                        selected={
                            location.pathname.toLowerCase().includes('/post') ||
                            location.pathname.toLowerCase().includes('/post/create') ||
                            location.pathname.toLowerCase().includes('/post/edit')
                        }
                        style={{ color: 'black' }}
                    >
                        <ListItemButton>
                            <ListItemIcon
                                sx={{
                                    minWidth: 0,
                                    mr: open ? 3 : 'auto',
                                    justifyContent: 'center',
                                }}
                            >
                                <PostAddIcon />
                            </ListItemIcon>
                            <ListItemText primary="Posts" sx={{ opacity: open ? 1 : 0 }} />
                        </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding sx={{ display: 'block' }} component={ReactRouterLink} to="/settings"
                        selected={'/settings' === location.pathname} style={{ color: 'black' }}>
                        <ListItemButton>
                            <ListItemIcon
                                sx={{
                                    minWidth: 0,
                                    mr: open ? 3 : 'auto',
                                    justifyContent: 'center',
                                }}
                            >
                                <SettingsIcon />
                            </ListItemIcon>
                            <ListItemText primary="Settings" sx={{ opacity: open ? 1 : 0 }} />
                        </ListItemButton>
                    </ListItem>
                </List>
                {/* End List Item */}
            </Drawer>
        </>
    );
}

export default Sidebar;