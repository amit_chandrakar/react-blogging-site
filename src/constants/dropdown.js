export const languages = ['english', 'hindi'];

export const choices = ['yes', 'no'];

export const dateFormats = [
    {
        format: 'm-d-Y',
        example: '01-15-2023',
    },
    {
        format: 'Y-m-d',
        example: '2023-01-15',
    },
    {
        format: 'd.m.Y',
        example: '15.01.2023',
    },
    {
        format: 'm.d.Y',
        example: '01.15.2023'
    },
    {
        format: 'Y.m.d',
        example: '2023.01.15'
    },
    {
        format: 'd/m/Y',
        example: '15/01/2023'
    },
    {
        format: 'm/d/Y',
        example: '01/15/2023'
    },
    {
        format: 'Y/m/d',
        example: '2023/01/15'
    },
    {
        format: 'd/M/Y',
        example: '15/Jan/2023'
    },
    {
        format: 'd.M.Y',
        example: '15.Jan.2023'
    },
    {
        format: 'd-M-Y',
        example: '15-Jan-2023'
    },
    {
        format: 'd M Y',
        example: '15 Jan 2023'
    },
    {
        format: 'd F, Y',
        example: '15 January, 2023'
    },
    {
        format: 'D/M/Y',
        example: 'Sun/Jan/2023'
    },
    {
        format: 'D.M.Y',
        example: 'Sun.Jan.2023'
    },
    {
        format: 'D-M-Y',
        example: 'Sun-Jan-2023'
    },
    {
        format: 'D M Y',
        example: 'Sun Jan 2023'
    },
    {
        format: 'd D M Y',
        example: '15 Sun Jan 2023'
    },
    {
        format: 'D d M Y',
        example: 'Sun 15 Jan 2023'
    },
    {
        format: 'dS M Y',
        example: '15th Jan 2023'
    }
];

export const timeFormats = [
    {
        format: 'h:i A',
        example: '09:27 PM',
        display: '12 Hour'
    },
    {
        format: 'h:i a',
        example: '09:27 pm',
        display: '12 Hour'
    },
    {
        format: 'H:i',
        example: '21:27',
        display: '24 Hour'
    }
];

export const genders = ['Male', 'Female', 'Other'];