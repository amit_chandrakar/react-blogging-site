import React, { useState } from "react";

// Node packages
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import { useFormik } from "formik";

// Custom components
import Sidebar from "../../components/menu/Sidebar";
import { DrawerHeader } from "../../components/menu/SidebarData";
import { CategoryValidation } from "../../schemas/CategoryValidation";

// React MUI components
import { Button, Card, CardContent, FormControlLabel, Grid, Snackbar, Switch, TextField, Box } from "@mui/material";
import { ThemeProvider, useTheme } from '@mui/material/styles';

// Icons
import { IconButton } from '@mui/material';
import ClearIcon from '@mui/icons-material/Clear';
import SaveIcon from '@mui/icons-material/Save';
import CircularProgress from '@mui/material/CircularProgress';

export function Create()
{
    const theme = useTheme();
    const navigate = useNavigate(); // Navigation
    const [open, setOpen] = React.useState(false); // Used for Snackbar
    const [isSubmitting, setIsSubmitting] = useState(false); // Used for showing loader in submit button

    // Handle Snackbar close event
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };

    // Formik with Yup for form validation and submission
    const { values, handleBlur, handleChange, handleSubmit, errors, touched } = useFormik({
        enableReinitialize: true,
        initialValues: {
            name: '',
            status: false,
        },
        validationSchema: CategoryValidation,
        validateOnChange: true,
        validateOnBlur: false,
        onSubmit: async (values, action) => {
            setIsSubmitting(true);

            let newStatus = (values.status===true) ? 'active' : 'inactive';
            values = { ...values, status: newStatus};

            let response = await axios.post(`${process.env.REACT_APP_FIREBASE_BASE_URL}/categories/store`, values, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
            });

            if (response.statusText === "OK") {
                setOpen(true);

                setTimeout(() => {
                    navigate('/category');
                }, 1000);
            }

            setIsSubmitting(false);
        },
    });

    return (
        <ThemeProvider theme={theme}>

            <Snackbar
                open={open}
                autoHideDuration={6000}
                onClose={() => setOpen(false)}
                message="Data saved successfully"
                action={
                    <React.Fragment>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={handleClose}
                            edge="start"
                        >
                            <ClearIcon />
                        </IconButton>
                    </React.Fragment>
                }
            />

            <Box sx={{ display: 'flex' }}>
                {/* This is left sidebar data */}
                <Sidebar title="Create Category" />

                <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
                    {/* Don't remove this */}
                    <DrawerHeader />

                    <Grid>
                        <h3>Create Category</h3>
                        <Card>
                            <CardContent component="form" noValidate onSubmit={handleSubmit}>
                                {/* <form name="form" onSubmit={handleSubmit}> */}
                                <h4>Category details</h4>
                                <Grid container spacing={2}>
                                    <Grid item xs={12} md={8}>
                                        <TextField
                                            margin="normal"
                                            fullWidth
                                            id="name"
                                            label="Category name"
                                            name="name"
                                            value={values.name}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            error={touched.name && Boolean(errors.name)}
                                            helperText={touched.name && errors.name}
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={4}>
                                        <FormControlLabel
                                            sx={{ ml: 0, pl: 0 }}
                                            control={<Switch color="primary"/>}
                                            label="Status"
                                            labelPlacement="top"
                                            name="status"
                                            value={values.status}
                                            onChange={handleChange}
                                        />
                                    </Grid>
                                </Grid>
                                    <Button type="submit" startIcon={isSubmitting ? <CircularProgress color="inherit" size="1rem" /> : <SaveIcon />} variant="contained" sx={{ mt: 1 }} disabled={isSubmitting}>
                                        {isSubmitting ? 'Loading...' : 'Save'}
                                    </Button>
                                {/* </form> */}
                            </CardContent>
                        </Card>
                    </Grid>

                </Box>
            </Box>
        </ThemeProvider>
    );
}