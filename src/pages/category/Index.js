import React, { useState, useEffect, useCallback, useMemo } from "react";
import { useNavigate } from "react-router-dom";

// Node packages
import axios from 'axios';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import DataTable from 'react-data-table-component';

// Custom components
import LoadingSpinner from "../../components/LoadingSpinner";
import Sidebar from "../../components/menu/Sidebar";
import { DrawerHeader } from "../../components/menu/SidebarData";

// React MUI components
import { ThemeProvider } from '@mui/material/styles';
import { useTheme } from '@mui/material/styles';
import { Badge, Button, Grid, IconButton, TextField, Typography, Box } from "@mui/material";

// Icons
import SearchIcon from '@mui/icons-material/Search';
import CachedIcon from '@mui/icons-material/Cached';
import SortIcon from "@mui/icons-material/ArrowDownward";
import DeleteIcon from '@mui/icons-material/Delete';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import EditIcon from '@mui/icons-material/Edit';
import { Add } from "@mui/icons-material";

export function Index()
{
    const theme = useTheme();
    const navigate = useNavigate(); // Navigation
    const MySwal = withReactContent(Swal); // For SweetAlert
    const [categories, setCategories] = useState([]); // For storing category data. Format: array of objects
    const [filterText, setFilterText] = useState(''); // For storing filter text
    const [pending, setPending] = useState(true); // Stores dataTable loader data
    const [selectedRows, setSelectedRows] = useState([]); // Stores table's selected rows information
    const [toggleCleared, setToggleCleared] = useState(false); // Contain if table data is selected or not

    // DataTables columns array having all the options
    const tableColumns = [
        {
            name: "Name",
            sortable: true,
            selector: row => row.name,
        },
        {
            name: "Status",
            sortable: true,
            cell: row => {
                let color = (row.status === 'active') ? 'success' : 'error';
                return (
                    <>
                        <Typography variant="body2" gutterBottom sx={{ mr: 1 }}>
                            <Badge color={color} variant="dot" />
                        </Typography>
                        <Typography variant="body2" gutterBottom >
                            {row.status}
                        </Typography>
                    </>
                )
            },
        },
        {
            name: "Actions",
            sortable: false,
            center: true,
            selector: row => {
                return (
                    <>
                        <Button
                            sx={{ mr: 1 }}
                            size="small"
                            variant="outlined"
                            startIcon={<RemoveRedEyeIcon />}
                            onClick={() => navigate(`/category/show/${row.id}`)}
                        >
                            View
                        </Button>
                        <Button
                            sx={{ mr: 1 }}
                            size="small"
                            variant="outlined"
                            startIcon={<EditIcon />}
                            onClick={() => navigate(`/category/edit/${row.id}`)}
                        >
                            Edit
                        </Button>
                        <Button
                            sx={{ mr: 1 }}
                            size="small"
                            variant="outlined"
                            startIcon={<DeleteIcon />}
                            onClick={() => deleteHandler(row.id)}
                        >
                            Delete
                        </Button>
                    </>
                )
            },
        }
    ];

    // Fetch all the categories
    const getCategory = async () => {

        let loadedCategories = [];
        let categories;

        try {
            categories = await axios.get(`${process.env.REACT_APP_FIREBASE_BASE_URL}/categories`, {
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem("token")}`
                }
            });
        } catch (error) {
            console.error(error);
        }

        for (const key in categories.data.categories) {
            loadedCategories.push({
                id: categories.data.categories[key]._id,
                name: categories.data.categories[key].name,
                status: categories.data.categories[key].status,
            });
        }

        setCategories(loadedCategories);
        setPending(false); // Hide dataTable loading
    }

    // Apply filter in category table
    const applyFilter = () => {
        if (!filterText){
            getCategory();
            return false;
        }

        const results = categories.filter((item) => {
            return filterText &&
                (
                    item.name.toLowerCase().includes(filterText.toLowerCase()) ||
                    item.status.toLowerCase().includes(filterText.toLowerCase())
                )
        });

        setCategories(results);
    }

    // Delete single category
    const deleteHandler = async (id) => {
        MySwal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                const url = `${process.env.REACT_APP_FIREBASE_BASE_URL}/categories/delete/${id}`;

                axios
                .delete(url, {
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem("token")}`,
                    },
                })
                .then(result => {
                    getCategory();
                    MySwal.fire(
                        'Deleted!',
                        'Category has been deleted successfully.',
                        'success'
                    );
                })
                .catch(error => {
                    MySwal.fire(
                        'Unexpected error!',
                        'Category deletion failed.',
                        'error'
                    );
                });
            }
        })
    }

    // Get selected row's data
    const handleRowSelected = useCallback(state => {
        setSelectedRows(state.selectedRows);
    }, []);

    // DataTable multiple row options and actions
    const contextActions = useMemo(() =>
    {
        const handleDelete = () => {
            MySwal.fire({
                title: 'Are you sure you want to bulk delete Categories?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {

                    selectedRows.map(row => {
                        var url = `${process.env.REACT_APP_FIREBASE_BASE_URL}/categories/${row.id}.json`;

                        axios.delete(url, {
                            headers: {
                                'Authorization': `Bearer ${localStorage.getItem("token")}`
                            }
                        })
                        .then(res => {
                            getCategory();
                        })
                        .catch(err => {
                            console.log(err);
                        });
                    })

                    MySwal.fire(
                        'Deleted!',
                        'Category has been deleted successfully.',
                        'success'
                    );

                    setToggleCleared(!toggleCleared);
                }
            })
        };

        return (
            <>
                {/* Multiple buttons/dropdown can be provided here */}
                <Button sx={{ mr: 1 }} key="delete" variant="outlined" onClick={handleDelete}>Delete</Button>
            </>
        );
    }, [categories, selectedRows, toggleCleared]);

    // Runs when component load
    useEffect(() => {
        getCategory(); // Get all the categories after page loading
    }, []);

    return (
        <ThemeProvider theme={theme}>
            <Box sx={{ display: 'flex' }}>

                {/* This is left sidebar data */}
                <Sidebar title="Categories" />

                <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
                    {/* This is drawer content */}
                    <DrawerHeader />

                    <Grid sx={{ mb: 3 }} container >
                        <Grid item xs={6} md={6} display="flex" justifyContent="flex-start">
                            <Button startIcon={<Add />} variant="contained" onClick={() => navigate('/category/create')}>Add Category</Button>
                            <Button sx={{ ml: 2 }} startIcon={<CachedIcon />} variant="outlined" onClick={() => { getCategory(); setFilterText(''); }}>Reload</Button>
                        </Grid>
                        <Grid item xs={6} md={6} display="flex" justifyContent="flex-end">
                            <TextField
                                align="right"
                                id="search"
                                size="small"
                                placeholder="Search Here"
                                value={filterText}
                                onChange={e => setFilterText(e.target.value)}
                                InputProps={{
                                    endAdornment:
                                        <>
                                            <IconButton onClick={applyFilter}>
                                                <SearchIcon />
                                            </IconButton>
                                        </>
                                }}
                            />
                        </Grid>
                    </Grid>

                    <Grid>
                        <DataTable
                            title="Categories"
                            columns={tableColumns}
                            data={categories}
                            direction="auto"
                            defaultSortField="name"
                            sortIcon={<SortIcon />}
                            responsive
                            highlightOnHover
                            pointerOnHover
                            pagination
                            selectableRows
                            contextActions={contextActions}
                            onSelectedRowsChange={handleRowSelected}
                            clearSelectedRows={toggleCleared}
                            progressPending={pending}
                            progressComponent={<LoadingSpinner />}
                        />
                    </Grid>
                </Box>
            </Box>
        </ThemeProvider>
    );
}