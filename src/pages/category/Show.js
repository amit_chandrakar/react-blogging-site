import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

// Node packages
import axios from 'axios';

// Custom components
import Sidebar from "../../components/menu/Sidebar";
import { DrawerHeader } from "../../components/menu/SidebarData";

// React MUI components
import { ThemeProvider, useTheme } from '@mui/material/styles';
import { Badge, Card, CardContent, Grid, Box } from "@mui/material";

export function Show()
{
    const theme = useTheme();
    const category = useParams('id'); // Get query parameter
    const [name, setName] = useState('');
    const [status, setStatus] = useState('inactive');

    // Get all the categories
    const getCategory = async () => {
        const url = `${process.env.REACT_APP_FIREBASE_BASE_URL}/categories/edit/${category.id}`;
        let result = await axios.get(url, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem("token")}`
            }
        });

        setName(result.data.data.category.name);
        setStatus(result.data.data.category.status);
    }

    // Runs when component load
    useEffect(() => {
        getCategory(); // Get all the categories
    }, []);

    return (
        <ThemeProvider theme={theme}>

            <Box sx={{ display: 'flex' }}>
                {/* This is left sidebar data */}
                <Sidebar title="Show Category" />

                <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
                    {/* Don't remove this */}
                    <DrawerHeader />

                    <Grid>
                        <h3>Category Details</h3>
                        <Card>
                            <CardContent>
                                <Grid container spacing={2}>
                                    <Grid item xs={6} md={6}>
                                        Category Name
                                    </Grid>
                                    <Grid item xs={6} md={6}>
                                        {name}
                                    </Grid>
                                    <Grid item xs={6} md={6}>
                                        Status
                                    </Grid>
                                    <Grid item xs={6} md={6}>
                                        <Badge sx={{ ml: 3 }} badgeContent={status.charAt(0).toUpperCase() + status.slice(1)} color={(status === 'active') ? 'primary' : 'error'} />
                                    </Grid>
                                </Grid>
                            </CardContent>
                        </Card>
                    </Grid>

                </Box>
            </Box>
        </ThemeProvider>
    );
}