import React, { useState, useEffect } from "react";
import { ThemeProvider } from '@mui/material/styles';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Sidebar from "../../components/menu/Sidebar";
import { DrawerHeader } from "../../components/menu/SidebarData";
import Grid from '@mui/material/Grid'; // Grid version 1
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import axios from 'axios';

export default function Dashboard()
{
    const theme = useTheme();
    const [totalCategories, setTotalCategories] = useState(0);
    const [totalSubCategories, setTotalSubCategories] = useState(0);
    const [totalPosts, setTotalPosts] = useState(0);
    const [totalPublishedPosts, setTotalPublishedPosts] = useState(0);
    const [totalUnPublishedPosts, setTotalUnPublishedPosts] = useState(0);

    const getCategory = async () => {
        let categories = await axios.get(`${process.env.REACT_APP_FIREBASE_BASE_URL}/categories/total-categories`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem("token")}`
            }
        });

        setTotalCategories(categories.data.totalCategories);
    }

    const getSubCategory = async () => {
        let subCategories = await axios.get(`${process.env.REACT_APP_FIREBASE_BASE_URL}/sub-categories/total-sub-categories`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem("token")}`
            }
        });
        setTotalSubCategories(subCategories.data.totalSubCategories);
    }

    const getPost = async () => {
        let posts = await axios.get(`${process.env.REACT_APP_FIREBASE_BASE_URL}/posts`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem("token")}`
            }
        });
        setTotalPosts(Object.keys(posts.data.posts).length);

        let totalActivePost = 0;
        for (const key in posts.data.posts) {
            if (posts.data.posts[key].status === 'active'){
                totalActivePost++;
            }
        }
        setTotalPublishedPosts(totalActivePost);

        let totalInActivePost = 0;
        for (const key in posts.data.posts) {
            if (posts.data.posts[key].status === 'inactive'){
                totalInActivePost++;
            }
        }
        setTotalUnPublishedPosts(totalInActivePost);
    }

    useEffect(() => {
        getCategory();
        getSubCategory();
        getPost();
    }, []);

    return (
        <ThemeProvider theme={theme}>
            <Box sx={{ display: 'flex' }}>
                {/* This is left sidebar data */}
                <Sidebar title="Dashboard" />

                <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
                    <DrawerHeader />
                    <Grid container>
                        <Grid xs={2} item={true}>
                            <Card>
                                <CardContent>
                                    <Typography variant="h2" >
                                        {totalCategories}
                                    </Typography>
                                    <Typography component="div">
                                      Total Categories
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid xs={2} sx={{ ml: 2 }} item={true}>
                            <Card>
                                <CardContent>
                                    <Typography variant="h2" >
                                        {totalSubCategories}
                                    </Typography>
                                    <Typography component="div">
                                      Total Sub Categories
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid xs={2} sx={{ ml: 2 }} item={true}>
                            <Card>
                                <CardContent>
                                    <Typography variant="h2" >
                                        {totalPosts}
                                    </Typography>
                                    <Typography component="div">
                                      Total Post
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid xs={2} sx={{ ml: 2 }} item={true}>
                            <Card>
                                <CardContent>
                                    <Typography variant="h2" >
                                        {totalPublishedPosts}
                                    </Typography>
                                    <Typography component="div">
                                      Total Published Post
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid xs={2} sx={{ ml: 2 }} item={true}>
                            <Card>
                                <CardContent>
                                    <Typography variant="h2" >
                                        {totalUnPublishedPosts}
                                    </Typography>
                                    <Typography component="div">
                                      Total Un-Published Post
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </Box>
            </Box>
        </ThemeProvider>
    );
}