import Navbar from "./../../components/front-website-layouts/Navbar";
import Footer from "./../../components/front-website-layouts/Footer";
import Header from "./../../components/front-website-layouts/Header";
import RazorpayPayment from "./../../components/front-website/payment-gateway/RazorpayPayment";
import StripePayment from "./../../components/front-website/payment-gateway/StripePayment";

function BuyMeCoffee() {

    return (
        <>
            <Navbar />
            <Header />

            <main className="mb-4">
                <div className="container px-4 px-lg-5">
                    <div className="row gx-4 gx-lg-5 justify-content-center">
                        <div className="col-md-10 col-lg-8 col-xl-7">
                            <p>
                                If you like my work, please consider buying me a
                                coffee. I will appreciate it very much!
                            </p>
                            <div className="row">
                                <RazorpayPayment />
                                <StripePayment />
                            </div>
                        </div>
                    </div>
                </div>
            </main>

            <Footer />
        </>
    );
}

export default BuyMeCoffee;
