import React, { useState, useEffect } from "react";
import axios from "axios";

import Navbar from "./../../components/front-website-layouts/Navbar";
import Footer from "./../../components/front-website-layouts/Footer";
import Header from "./../../components/front-website-layouts/Header";
import Blog from "./../../components/front-website/Blog";

import LoadingSpinner from "../../components/LoadingSpinner";

function Home() {
    const [posts, setPosts] = useState([]); // For storing posts data. Format: array of objects
    const [pending, setPending] = useState(true); // Stores dataTable loader data

    // Fetch all the posts
    const getPost = async () => {
        let loadedCategories = [];
        let posts = await axios.get(
            `${process.env.REACT_APP_FIREBASE_BASE_URL}/posts`,
            {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
            }
        );

        for (const key in posts.data.posts) {
            loadedCategories.push({
                id: posts.data.posts[key].id,
                title: posts.data.posts[key].title,
                slug: posts.data.posts[key].slug,
                sub_title: posts.data.posts[key].sub_title,
                long_description: posts.data.posts[key].long_description,
                status: posts.data.posts[key].status,
                category: posts.data.posts[key].category[0].name,
                sub_category: posts.data.posts[key].subcategory[0].name,
            });
        }

        setPosts(loadedCategories);
        setPending(false); // Hide dataTable loading
    };

    // Runs when component load
    useEffect(() => {
        getPost(); // Get all the posts after page loading
    }, []);

    return (
        <>
            <Navbar />
            <Header />

            <div className="container px-4 px-lg-5">
                <div className="row gx-4 gx-lg-5 justify-content-center">
                    <div className="col-md-10 col-lg-8 col-xl-7">
                        {pending && (
                            <LoadingSpinner message="Loading Posts..." />
                        )}

                        {posts.length > 0 &&
                            posts.map((post, index) => {
                                return <Blog post={post} key={index} />;
                            })}

                        <div className="d-flex justify-content-end mb-4">
                            <a
                                className="btn btn-primary text-uppercase"
                                href="#!"
                            >
                                Older Posts →
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <Footer />
        </>
    );
}

export default Home;
