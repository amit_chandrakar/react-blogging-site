import React, { useState, useEffect } from "react";
import axios from "axios";

import Navbar from "./../../components/front-website-layouts/Navbar";
import Footer from "./../../components/front-website-layouts/Footer";
import Header from "./../../components/front-website-layouts/Header";
import { useParams } from "react-router";
import LoadingSpinner from "../../components/LoadingSpinner";
import parse from "html-react-parser";

function Post() {
    const slug = useParams("slug"); // Get query parameter
    const [post, setPost] = useState({}); // For storing posts data. Format: array of objects
    const [pending, setPending] = useState(true); // Stores dataTable loader data

    // Fetch all the posts
    const getPost = async () => {
        let post = await axios.get(
            `${process.env.REACT_APP_FIREBASE_BASE_URL}/posts/show/${slug.slug}`,
            {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
            }
        );

        setPost(post.data.data.post);
        setPending(false);
    };

    // Runs when component load
    useEffect(() => {
        getPost(); // Get all the posts after page loading
    }, []);

    return (
        <>
            <Navbar />
            <Header />

            <article className="mb-4">
                <div className="container px-4 px-lg-5">
                    <div className="row gx-4 gx-lg-5 justify-content-center">
                        <div className="col-md-10 col-lg-8 col-xl-7">
                            {pending && (
                                <LoadingSpinner message="Loading Blog..." />
                            )}

                            {post !== null && (
                                <>
                                    <h2 className="section-heading">
                                        {post.title}
                                    </h2>
                                    <br />
                                    <h5>{post.sub_title}</h5>
                                    <p>
                                    {parse(String(post.longDescription))}
                                    </p>
                                </>
                            )}
                        </div>
                    </div>
                </div>
            </article>

            <Footer />
        </>
    );
}

export default Post;
