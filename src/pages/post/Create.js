import React, { useState, useEffect } from "react";

// Node packages
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { useFormik } from "formik";

// Custom components
import Sidebar from "../../components/menu/Sidebar";
import { DrawerHeader } from "../../components/menu/SidebarData";

// React MUI components
import { ThemeProvider, useTheme } from '@mui/material/styles';
import { Autocomplete, Button, Card, CardContent, FormControlLabel, Grid, Snackbar, Switch, TextField, Box } from "@mui/material";

// Icons
import { IconButton } from '@mui/material';
import ClearIcon from '@mui/icons-material/Clear';
import SaveIcon from '@mui/icons-material/Save';
import CircularProgress from '@mui/material/CircularProgress';
import { PostValidation } from "../../schemas/PostValidation";

const config = {
    toolbar: {
        // items: ['bold', 'italic', '|', 'undo', 'redo', '-', 'numberedList', 'bulletedList'],
        shouldNotGroupWhenFull: true
    }
};

export function Create() {
    const theme = useTheme();
    const navigate = useNavigate();
    const [open, setOpen] = React.useState(false);
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [categories, setCategories] = useState([]);
    const [subCategories, setSubCategories] = useState([]);

    // Handle Snackbar close event
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };

    // Fetch all the categories
    const getCategory = async () => {
        let loadedCategories = [];
        let categories = await axios.get(`${process.env.REACT_APP_FIREBASE_BASE_URL}/categories`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem("token")}`
            }
        });

        for (const key in categories.data.categories) {
            loadedCategories.push({
                id: categories.data.categories[key]._id,
                name: categories.data.categories[key].name
            });
        }

        setCategories(loadedCategories);
    }

    // Fetch all the sub-categories for passed categoryName
    const getSubCategory = async (categoryId) => {
        let loadedCategories = [];
        let categories = await axios.get(`${process.env.REACT_APP_FIREBASE_BASE_URL}/sub-categories/get-sub-categories-by-category-id/${categoryId}`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem("token")}`
            }
        });

        for (const key in categories.data.subCategory) {
            loadedCategories.push(categories.data.subCategory[key]);
        }

        setSubCategories(loadedCategories);
    }

    // Runs when component load
    useEffect(() => {
        getCategory(); // Get all the categories after page loading
    }, []);

    // Formik with Yup for form validation and submission
    const { values, handleBlur, handleChange, handleSubmit, setFieldValue, errors, touched } = useFormik({
        enableReinitialize: true,
        initialValues: {
            category: '',
            sub_category: '',
            title: '',
            sub_title: '',
            long_description: '',
            status: false,
        },
        validationSchema: PostValidation,
        validateOnChange: true,
        validateOnBlur: false,
        onSubmit: async (values, action) => {
            setIsSubmitting(true);

            let newStatus = (values.status === true) ? 'active' : 'inactive';
            values = { ...values, status: newStatus, categoryId: values.category, subCategoryId: values.sub_category, subTitle: values.sub_title, longDescription: values.long_description };

            let response = await axios.post(`${process.env.REACT_APP_FIREBASE_BASE_URL}/posts/store`, values, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
            });

            if (response.statusText === "OK") {
                setOpen(true);
                setTimeout(() => {
                    navigate('/post');
                }, 1000);
            }
            setIsSubmitting(false);
        },
    });

    return (
        <ThemeProvider theme={theme}>

            <Snackbar
                open={open}
                autoHideDuration={6000}
                onClose={() => setOpen(false)}
                message="Data saved successfully"
                action={
                    <React.Fragment>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={handleClose}
                            edge="start"
                        >
                            <ClearIcon />
                        </IconButton>
                    </React.Fragment>
                }
            />

            <Box sx={{ display: 'flex' }}>
                {/* This is left sidebar data */}
                <Sidebar title="Create Post" />

                <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
                    {/* Don't remove this */}
                    <DrawerHeader />

                    <Grid item xs={12} md={12}>
                        <h3>Create Post</h3>
                        <Card>
                            <CardContent component="form" noValidate onSubmit={handleSubmit}>
                                <h4>Post Details</h4>
                                <Grid container spacing={2}>
                                    <Grid item xs={12} md={6}>
                                        <Autocomplete
                                            disablePortal
                                            name="category"
                                            id="category"
                                            options={categories}
                                            getOptionLabel={option => option.name}
                                            onChange={(event, value) => {
                                                getSubCategory(value.id);
                                                setFieldValue("category", value.id);
                                            }}
                                            renderInput={(params) =>
                                                <TextField
                                                    {...params}
                                                    label="Category Name"
                                                    error={touched.category && Boolean(errors.category)}
                                                    helperText={touched.category && errors.category}
                                                />
                                            }
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={6}>
                                        <Autocomplete
                                            disablePortal
                                            name="sub_category"
                                            id="sub_category"
                                            options={subCategories}
                                            getOptionLabel={option => option.name}
                                            onChange={(event, value) => setFieldValue("sub_category", value._id)}
                                            renderInput={(params) =>
                                                <TextField
                                                    {...params}
                                                    label="Sub-Category Name"
                                                    error={touched.sub_category && Boolean(errors.sub_category)}
                                                    helperText={touched.sub_category && errors.sub_category}
                                                />
                                            }
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={12}>
                                        <TextField
                                            sx={{ mt: 0 }}
                                            margin="normal"
                                            fullWidth
                                            id="title"
                                            label="Title"
                                            name="title"
                                            value={values.title || ''}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            error={touched.title && Boolean(errors.title)}
                                            helperText={touched.title && errors.title}
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={12}>
                                        <TextField
                                            sx={{ mt: 0 }}
                                            margin="normal"
                                            fullWidth
                                            id="name"
                                            label="Sub-Title"
                                            name="sub_title"
                                            value={values.sub_title || ''}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            error={touched.sub_title && Boolean(errors.sub_title)}
                                            helperText={touched.sub_title && errors.sub_title}
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={12}>
                                        <CKEditor
                                            config={config}
                                            id="long_description"
                                            description="long_description"
                                            editor={ClassicEditor}
                                            name="long_description"
                                            data={values.long_description || ''}
                                            onChange={(event, editor) => {
                                                const data = editor.getData();
                                                setFieldValue("long_description", data)
                                            }}
                                            error={touched.long_description && Boolean(errors.long_description)}
                                            helperText={touched.long_description && errors.long_description}
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={4}>
                                        <FormControlLabel
                                            sx={{ ml: 0, pl: 0 }}
                                            control={<Switch color="primary" />}
                                            label="Status"
                                            labelPlacement="top"
                                            name="status"
                                            value={values.status}
                                            onChange={handleChange}
                                        />
                                    </Grid>
                                </Grid>
                                <Button type="submit" startIcon={isSubmitting ? <CircularProgress color="inherit" size="1rem" /> : <SaveIcon />} variant="contained" sx={{ mt: 1 }} disabled={isSubmitting}>
                                    {isSubmitting ? 'Loading...' : 'Save'}
                                </Button>
                            </CardContent>
                        </Card>
                    </Grid>

                </Box>
            </Box>
        </ThemeProvider>
    );
}