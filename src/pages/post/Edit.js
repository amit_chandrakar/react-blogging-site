import React, { useState, useEffect } from "react";

// Node packages
import { useNavigate, useParams } from "react-router-dom";
import axios from 'axios';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { useFormik } from "formik";

// Custom components
import Sidebar from "../../components/menu/Sidebar";
import { DrawerHeader } from "../../components/menu/SidebarData";

// React MUI components
import { ThemeProvider, useTheme } from '@mui/material/styles';
import { Button, Card, CardContent, FormControlLabel, Grid, Snackbar, Switch, TextField, Box } from "@mui/material";

// Icons
import { IconButton } from '@mui/material';
import ClearIcon from '@mui/icons-material/Clear';
import SaveIcon from '@mui/icons-material/Save';
import CircularProgress from '@mui/material/CircularProgress';
import { PostValidation } from "../../schemas/PostValidation";

const config = {
    toolbar: {
        // items: ['bold', 'italic', '|', 'undo', 'redo', '-', 'numberedList', 'bulletedList'],
        shouldNotGroupWhenFull: true
    }
};

export function Edit()
{
    const theme = useTheme();
    const navigate = useNavigate();
    const [open, setOpen] = React.useState(false);
    const post = useParams('id');
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [initialValues, setInitialValues] = useState({}); // Form initial values

    useEffect(() => {
        getPost();
    }, []);

    const getPost = async () => {
        const url = `${process.env.REACT_APP_FIREBASE_BASE_URL}/posts/edit/${post.id}`;
        let result = await axios.get(url, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem("token")}`
            }
        });

        // Fetch category name
        let category = await axios.get(`${process.env.REACT_APP_FIREBASE_BASE_URL}/categories/edit/${result.data.post.category_id}`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem("token")}`
            }
        });
        // Fetch sub-category name
        let subCategory = await axios.get(`${process.env.REACT_APP_FIREBASE_BASE_URL}/sub-categories/edit/${result.data.post.sub_category_id}`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem("token")}`
            }
        });

        result = result.data.post;
        let newStatus = (result.status == 'active') ? true : false;

        setInitialValues({ ...result, status: newStatus, category: category.data.category.name, subCategory: subCategory.data.subCategory.name });
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };

    // Formik with Yup for form validation and submission
    const { values, handleBlur, handleChange, setFieldValue, handleSubmit, errors, touched } = useFormik({
        enableReinitialize: true,
        initialValues: initialValues,
        validationSchema: PostValidation,
        validateOnChange: true,
        validateOnBlur: false,
        onSubmit: async (values, action) => {
            setIsSubmitting(true);

            let newStatus = (values.status === true) ? 'active' : 'inactive';
            values = { ...values, status: newStatus };

            let response = await axios.put(`${process.env.REACT_APP_FIREBASE_BASE_URL}/posts/update/${post.id}`, values, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
            });

            if (response.statusText === "OK") {
                setOpen(true);
                setTimeout(() => {
                    navigate('/post');
                }, 1000);
            }
            setIsSubmitting(false);
        },
    });

    return (
        <ThemeProvider theme={theme}>

            <Snackbar
                open={open}
                autoHideDuration={6000}
                onClose={() => setOpen(false)}
                message="Data saved successfully"
                action={
                    <React.Fragment>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={handleClose}
                            edge="start"
                        >
                            <ClearIcon />
                        </IconButton>
                    </React.Fragment>
                }
            />

            <Box sx={{ display: 'flex' }}>
                {/* This is left sidebar data */}
                <Sidebar title="Edit Post" />

                <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
                    {/* Don't remove this */}
                    <DrawerHeader />

                    <Grid>
                        <h3>Edit Category</h3>
                        <Card>
                            <CardContent component="form" noValidate onSubmit={handleSubmit}>
                                <h4>Sub-Category Details</h4>
                                <Grid container spacing={2}>
                                    <Grid item xs={12} md={6}>
                                        <TextField
                                            sx={{ mt: 0 }}
                                            margin="normal"
                                            fullWidth
                                            label="Category Name"
                                            value={initialValues.category || ''}
                                            disabled
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={6}>
                                        <TextField
                                            sx={{ mt: 0 }}
                                            margin="normal"
                                            fullWidth
                                            label="Sub Category Name"
                                            value={initialValues.subCategory || ''}
                                            disabled
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={12}>
                                        <TextField
                                            sx={{ mt: 0 }}
                                            margin="normal"
                                            fullWidth
                                            id="title"
                                            label="Post Title"
                                            name="title"
                                            autoComplete="title"
                                            value={values.title || ''}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            error={touched.title && Boolean(errors.title)}
                                            helperText={touched.title && errors.title}
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={12}>
                                        <CKEditor
                                            config={config}
                                            id="description"
                                            description="name"
                                            editor={ClassicEditor}
                                            data={values.description}
                                            onChange={(event, editor) => {
                                                const data = editor.getData();
                                                setFieldValue("description", data)
                                            }}
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={4}>
                                        <FormControlLabel
                                            sx={{ ml: 0, pl: 0 }}
                                            control={<Switch color="primary" checked={(values.status) ? true : false} />}
                                            label="Status"
                                            labelPlacement="top"
                                            name="status"
                                            value={values.status || ''}
                                            onChange={handleChange}
                                        />
                                    </Grid>
                                </Grid>
                                <Button type="submit" startIcon={isSubmitting ? <CircularProgress color="inherit" size="1rem" /> : <SaveIcon />} variant="contained" sx={{ mt: 1 }} disabled={isSubmitting}>
                                    {isSubmitting ? 'Loading...' : 'Save'}
                                </Button>
                            </CardContent>
                        </Card>
                    </Grid>

                </Box>
            </Box>
        </ThemeProvider>
    );
}