import React, { useState, useEffect, useCallback, useMemo } from "react";
import { useNavigate } from "react-router-dom";

// Node packages
import axios from 'axios';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import DataTable from 'react-data-table-component';

// Custom components
import LoadingSpinner from "../../components/LoadingSpinner";
import Sidebar from "../../components/menu/Sidebar";
import { DrawerHeader } from "../../components/menu/SidebarData";

// React MUI components
import { ThemeProvider } from '@mui/material/styles';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import { Badge, Button, Grid, IconButton, TextField, Typography } from "@mui/material";

// Icons
import SearchIcon from '@mui/icons-material/Search';
import CachedIcon from '@mui/icons-material/Cached';
import SortIcon from "@mui/icons-material/ArrowDownward";
import DeleteIcon from '@mui/icons-material/Delete';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import EditIcon from '@mui/icons-material/Edit';
import { Add } from "@mui/icons-material";

export function Index()
{
    const theme = useTheme();
    const navigate = useNavigate(); // Navigation
    const MySwal = withReactContent(Swal); // For SweetAlert
    const [posts, setPosts] = useState([]); // For storing posts data. Format: array of objects
    const [filterText, setFilterText] = useState(''); // For storing filter text
    const [pending, setPending] = useState(true); // Stores dataTable loader data
    const [selectedRows, setSelectedRows] = useState([]); // Stores table's selected rows information
    const [toggleCleared, setToggleCleared] = useState(false); // Contain if table data is selected or not

    // DataTables columns array having all the options
    const tableColumns = [
        {
            name: "Category",
            width: "10%",
            sortable: true,
            selector: row => row.category,
        },
        {
            name: "Sub-Category",
            width: "10%",
            sortable: true,
            selector: row => row.sub_category,
        },
        {
            name: "Post Title",
            width: "30%",
            sortable: true,
            selector: row => row.title,
        },
        {
            name: "Status",
            width: "10%",
            sortable: true,
            selector: row => row.status,
            cell: row => {
                let color = (row.status === 'active') ? 'success' : 'error';
                return (
                    <>
                        <Typography variant="body2" gutterBottom sx={{ mr: 1 }}>
                            <Badge color={color} variant="dot" />
                        </Typography>
                        <Typography variant="body2" gutterBottom >
                            {row.status.charAt(0).toUpperCase() + row.status.slice(1)}
                        </Typography>
                    </>
                )
            },
        },
        {
            name: "Actions",
            width: "30%",
            center: true,
            sortable: false,
            cell: row => {
                return (
                    <>
                        <Button
                            sx={{ mr: 1 }}
                            size="small"
                            variant="outlined"
                            startIcon={<RemoveRedEyeIcon />}
                            onClick={() => navigate(`/post/show/${row.id}`)}
                        >
                            View
                        </Button>
                        <Button
                            sx={{ mr: 1 }}
                            size="small"
                            variant="outlined"
                            startIcon={<EditIcon />}
                            onClick={() => navigate(`/post/edit/${row.id}`)}
                        >
                            Edit
                        </Button>
                        <Button
                            sx={{ mr: 1 }}
                            size="small"
                            variant="outlined"
                            startIcon={<DeleteIcon />}
                            onClick={() => deleteHandler(row.id)}
                        >
                            Delete
                        </Button>
                    </>
                )
            },
        }
    ];

    // Fetch all the posts
    const getPost = async () => {
        let loadedCategories = [];
        let posts = await axios.get(`${process.env.REACT_APP_FIREBASE_BASE_URL}/posts`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem("token")}`
            }
        });

        for (const key in posts.data.posts) {
            loadedCategories.push({
                id: posts.data.posts[key]._id,
                category: posts.data.posts[key].category[0].name,
                sub_category: posts.data.posts[key].subcategory[0].name,
                title: posts.data.posts[key].title,
                status: posts.data.posts[key].status,
            });
        }

        setPosts(loadedCategories);
        setPending(false); // Hide dataTable loading
    }

    // Apply filter in category table
    const applyFilter = () => {
        if (!filterText) {
            getPost();
            return false;
        }

        const results = posts.filter((item) => {
            return filterText &&
                (
                    item.sub_category.toLowerCase().includes(filterText.toLowerCase()) ||
                    item.title.toLowerCase().includes(filterText.toLowerCase()) ||
                    item.status.toLowerCase().includes(filterText.toLowerCase()) ||
                    item.category.toLowerCase().includes(filterText.toLowerCase())
                )
        });

        setPosts(results);
    }

    // Delete single post
    const deleteHandler = async (id) => {
        MySwal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                const url = `${process.env.REACT_APP_FIREBASE_BASE_URL}/posts/delete/${id}`;

                axios
                .delete(url, {
                    headers: {
                        'Authorization': `Bearer ${localStorage.getItem("token")}`
                    }
                })
                .then(result => {
                    getPost();
                    MySwal.fire(
                        'Deleted!',
                        'Post has been deleted successfully.',
                        'success'
                    );
                })
                .catch(error => {
                    MySwal.fire(
                        'Unexpected error!',
                        'Post deletion failed.',
                        'error'
                    );
                });
            }
        })
    }

    // Get selected row's data
    const handleRowSelected = useCallback(state => {
        setSelectedRows(state.selectedRows);
    }, []);

    // DataTable multiple row options and actions
    const contextActions = useMemo(() => {
        const handleDelete = () => {
            MySwal.fire({
                title: 'Are you sure you want to bulk delete Sub-Categories?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {

                    selectedRows.map(row => {
                        var url = `${process.env.REACT_APP_FIREBASE_BASE_URL}/posts/delete/${row.id}`;

                        axios.delete(url, {
                            headers: {
                                'Authorization': `Bearer ${localStorage.getItem("token")}`
                            }
                        })
                            .then(res => {
                                getPost();
                            })
                            .catch(err => {
                                console.log(err);
                            });
                    })

                    MySwal.fire(
                        'Deleted!',
                        'Category has been deleted successfully.',
                        'success'
                    );

                    setToggleCleared(!toggleCleared);
                }
            })
        };

        return (
            <>
                {/* Multiple buttons/dropdown can be provided here */}
                <Button sx={{ mr: 1 }} key="delete" variant="outlined" onClick={handleDelete}>Delete</Button>
            </>
        );
    }, [posts, selectedRows, toggleCleared]);

    // Runs when component load
    useEffect(() => {
        getPost(); // Get all the posts after page loading
    }, []);

    return (
        <ThemeProvider theme={theme}>
            <Box sx={{ display: 'flex' }}>

                {/* This is left sidebar data */}
                <Sidebar title="Posts" />

                <Box component="main" sx={{ flexGrow: 1, p: 3 }}>

                    {/* This is drawer content */}
                    <DrawerHeader />

                    <Grid sx={{ mb: 3 }} container >
                        <Grid item xs={6} md={6} display="flex" justifyContent="flex-start">
                            <Button startIcon={<Add />} variant="contained" onClick={() => navigate('/post/create')}>Add Post</Button>
                            <Button sx={{ ml: 2 }} startIcon={<CachedIcon />} variant="outlined" onClick={() => { getPost(); setFilterText(''); }}>Reload</Button>
                        </Grid>
                        <Grid item xs={6} md={6} display="flex" justifyContent="flex-end">
                            <TextField
                                align="right"
                                id="search"
                                size="small"
                                placeholder="Search Here"
                                value={filterText}
                                onChange={e => setFilterText(e.target.value)}
                                InputProps={{
                                    endAdornment:
                                        <>
                                            <IconButton onClick={applyFilter}>
                                                <SearchIcon />
                                            </IconButton>
                                        </>
                                }}
                            />
                        </Grid>
                    </Grid>

                    <Grid>
                        <DataTable
                            title="Posts"
                            columns={tableColumns}
                            data={posts}
                            direction="auto"
                            defaultSortField="name"
                            sortIcon={<SortIcon />}
                            responsive
                            highlightOnHover
                            pointerOnHover
                            pagination
                            selectableRows
                            contextActions={contextActions}
                            onSelectedRowsChange={handleRowSelected}
                            clearSelectedRows={toggleCleared}
                            progressPending={pending}
                            progressComponent={<LoadingSpinner />}
                        />
                    </Grid>

                </Box>
            </Box>
        </ThemeProvider>
    );
}