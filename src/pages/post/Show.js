import React, { useState, useEffect } from "react";
import {
    useParams
} from "react-router-dom";
import { ThemeProvider } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Sidebar from "../../components/menu/Sidebar";
import { DrawerHeader } from "../../components/menu/SidebarData";
import { useTheme } from '@mui/material/styles';
import { Badge, Card, CardContent,Grid} from "@mui/material";
import axios from 'axios';

export function Show() {
    const theme = useTheme();
    const [category, setCategory] = useState('');
    const [subCategory, setSubCategory] = useState('');
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [status, setStatus] = useState('inactive');
    const post = useParams('id');

    useEffect(() => {
        getPost();
    }, []);

    const getPost = async () => {
        const url = `${process.env.REACT_APP_FIREBASE_BASE_URL}/posts/edit/${post.id}`;
        let result = await axios.get(url, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem("token")}`
            }
        });

        setTitle(result.data.data.post.title);
        setDescription(result.data.data.post.longDescription);
        setStatus(result.data.data.post.status);
        setCategory(result.data.data.post.category[0].name);
        setSubCategory(result.data.data.post.subCategory[0].name);
    }

    return (
        <ThemeProvider theme={theme}>

            <Box sx={{ display: 'flex' }}>

                {/* This is left sidebar data */}
                <Sidebar title="Show Post" />

                <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
                    {/* Don't remove this */}
                    <DrawerHeader />

                    <Grid>
                        <h3>Post Details</h3>
                        <Card>
                            <CardContent>
                                <Grid container spacing={2}>

                                    <Grid item xs={4} md={4}>
                                        Category Name
                                    </Grid>
                                    <Grid item xs={8} md={8}>
                                        {category}
                                    </Grid>

                                    <Grid item xs={4} md={4}>
                                        Sub-Category Name
                                    </Grid>
                                    <Grid item xs={8} md={8}>
                                        {subCategory}
                                    </Grid>

                                    <Grid item xs={4} md={4}>
                                        Post Title
                                    </Grid>
                                    <Grid item xs={8} md={8}>
                                        {title}
                                    </Grid>

                                    <Grid item xs={4} md={4}>
                                        Post Description
                                    </Grid>
                                    <Grid item xs={8} md={8}>
                                        <div dangerouslySetInnerHTML={{ __html: description }} />
                                    </Grid>

                                    <Grid item xs={4} md={4}>
                                        Status
                                    </Grid>
                                    <Grid item xs={8} md={8}>
                                        <Badge sx={{ ml: 3 }} badgeContent={status} color={(status === 'active') ? 'primary' : 'error'} />
                                    </Grid>

                                </Grid>
                            </CardContent>
                        </Card>
                    </Grid>

                </Box>
            </Box>
        </ThemeProvider>
    );
}