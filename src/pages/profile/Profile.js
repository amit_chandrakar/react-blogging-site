import React, { useEffect, useState } from "react";

// Node packages
import axios from "axios";
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { useFormik } from "formik";

// Custom components
import Sidebar from "../../components/menu/Sidebar";
import { DrawerHeader } from "../../components/menu/SidebarData";
import { genders } from "../../constants/dropdown";
import { ProfileValidation } from "../../schemas/ProfileValidation";

// React MUI components
import { ThemeProvider, useTheme } from '@mui/material/styles';
import { Autocomplete, Button, Card, CardContent, Grid, IconButton, Snackbar, TextField, Box } from "@mui/material";

// Icons
import ClearIcon from '@mui/icons-material/Clear';
import SaveIcon from '@mui/icons-material/Save';
import CircularProgress from '@mui/material/CircularProgress';

export default function Profile()
{
    const theme = useTheme();
    const [open, setOpen] = useState(false);
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [initialValues, setInitialValues] = useState({}); // Form initial values

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };

    const getProfile = async () =>
    {
        // Get user data from local storage
        const user = JSON.parse(localStorage.getItem("user"));
        const url = `${process.env.REACT_APP_FIREBASE_BASE_URL}/users/edit/${user.id}`;
        let result = await axios.get(url, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem("token")}`
            }
        });
        setInitialValues(result.data.user);
    }

    useEffect(() => {
        getProfile();
    }, []);

    // Formik with Yup for form validation and submission
    const { values, handleBlur, handleChange, handleSubmit, setFieldValue, errors, touched } = useFormik({
        enableReinitialize: true,
        initialValues: initialValues,
        validationSchema: ProfileValidation,
        validateOnChange: true,
        validateOnBlur: false,
        onSubmit: async (values, action) => {
            setIsSubmitting(true);
            let response = await axios.put(`${process.env.REACT_APP_FIREBASE_BASE_URL}/users/update/${initialValues.id}`, values, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
            });

            if (response.statusText === "OK") {
                setOpen(true);
            }

            setIsSubmitting(false);
        },
    });

    return (
        <ThemeProvider theme={theme}>

            <Snackbar
                open={open}
                autoHideDuration={6000}
                onClose={() => setOpen(false)}
                message="Data updated successfully"
                action={
                    <React.Fragment>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={handleClose}
                            edge="start"
                        >
                            <ClearIcon />
                        </IconButton>
                    </React.Fragment>
                }
            />

            <Box sx={{ display: 'flex' }}>
                {/* This is left sidebar data */}
                <Sidebar title="Profile" />

                <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
                    {/* Don't remove this */}
                    <DrawerHeader />

                    <Grid>
                        <h3>Profile Settings</h3>
                        <Card>
                            <CardContent component="form" noValidate onSubmit={handleSubmit}>
                                <Grid container spacing={2}>
                                    <Grid item xs={12} md={4}>
                                        <TextField
                                            sx={{ mt: 2 }}
                                            margin="normal"
                                            fullWidth
                                            id="name"
                                            label="Name"
                                            name="name"
                                            value={values.name || ''}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            error={touched.name && Boolean(errors.name)}
                                            helperText={touched.name && errors.name}
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={4}>
                                        <TextField
                                            sx={{ mt: 2 }}
                                            margin="normal"
                                            fullWidth
                                            id="email"
                                            label="Email"
                                            name="email"
                                            type="email"
                                            value={values.email || ''}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            error={touched.email && Boolean(errors.email)}
                                            helperText={touched.email && errors.email}
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={4}>
                                        <TextField
                                            sx={{ mt: 2 }}
                                            margin="normal"
                                            fullWidth
                                            id="password"
                                            label="Password"
                                            name="password"
                                            type="password"
                                            value={values.password || ''}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            error={touched.password && Boolean(errors.password)}
                                            helperText={touched.password && errors.password}
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={4}>
                                        <TextField
                                            sx={{ mt: 0 }}
                                            margin="normal"
                                            fullWidth
                                            id="mobile"
                                            label="Mobile"
                                            name="mobile"
                                            autoComplete="mobile"
                                            type="number"
                                            value={values.mobile || ''}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            error={touched.mobile && Boolean(errors.mobile)}
                                            helperText={touched.mobile && errors.mobile}
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={4}>
                                        <LocalizationProvider dateAdapter={AdapterMoment}>
                                            <DatePicker
                                                sx={{ mt: 0 }}
                                                label="Date of Birth"
                                                inputFormat="DD/MM/YYYY"
                                                fullWidth
                                                id="dob"
                                                name="dob"
                                                onChange={(newValue) => {
                                                    setFieldValue("dob", newValue);
                                                }}
                                                value={values.dob || ''}
                                                renderInput={(params) =>
                                                    <TextField
                                                        {...params}
                                                        sx={{ width: '100%' }}
                                                        error={touched.dob && Boolean(errors.dob)}
                                                        helperText={touched.dob && errors.dob}
                                                    />
                                                }
                                            />
                                        </LocalizationProvider>
                                    </Grid>
                                    <Grid item xs={12} md={4}>
                                        <Autocomplete
                                            disablePortal
                                            name="gender"
                                            id="gender"
                                            options={genders}
                                            getOptionLabel={option => option.charAt(0).toUpperCase() + option.slice(1)}
                                            value={values.gender || ''}
                                            onChange={(event, selectedValue) => setFieldValue("gender", selectedValue)}
                                            renderInput={(params) =>
                                                <TextField
                                                    {...params}
                                                    label="Gender"
                                                    value={values.gender || ''}
                                                    error={touched.gender && Boolean(errors.gender)}
                                                    helperText={touched.gender && errors.gender}
                                                />
                                            }
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={12}>
                                        <TextField
                                            id="address"
                                            name="address"
                                            label="Address"
                                            multiline
                                            rows={5}
                                            fullWidth
                                            variant="outlined"
                                            value={values.address || ''}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            error={touched.address && Boolean(errors.address)}
                                            helperText={touched.address && errors.address}
                                        />
                                    </Grid>
                                </Grid>
                                <Button type="submit" startIcon={isSubmitting ? <CircularProgress color="inherit" size="1rem" /> : <SaveIcon />} variant="contained" sx={{ mt: 2 }} disabled={isSubmitting}>
                                    { isSubmitting ? 'Loading...' : 'Save' }
                                </Button>
                            </CardContent>
                        </Card>
                    </Grid>
                </Box>
            </Box>
        </ThemeProvider>
    );
}