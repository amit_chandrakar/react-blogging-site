import React, { useEffect, useState } from "react";

// Node packages
import axios from "axios";
import { useFormik } from "formik";

// Custom components
import Sidebar from "../../components/menu/Sidebar";
import { DrawerHeader } from "../../components/menu/SidebarData";
import {
    dateFormats,
    languages,
    timeFormats,
    choices,
} from "../../constants/dropdown";
import { SettingValidation } from "../../schemas/SettingValidation";

// React MUI components
import { ThemeProvider, useTheme } from "@mui/material/styles";
import {
    Autocomplete,
    Button,
    Card,
    CardContent,
    Grid,
    IconButton,
    Snackbar,
    TextField,
    Box,
    Stack,
} from "@mui/material";

// Icons
import ClearIcon from "@mui/icons-material/Clear";
import CircularProgress from "@mui/material/CircularProgress";
import SaveIcon from "@mui/icons-material/Save";

export default function Setting() {
    const theme = useTheme();
    const [open, setOpen] = useState(false);
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [initialValues, setInitialValues] = useState({}); // Form initial values
    const [imageUrl, setImageUrl] = useState(null);
    const [logo, setLogo] = useState();

    const handleFileUpload = (event) => {
        const file = event.target.files[0];
        setLogo(file);

        const reader = new FileReader();

        reader.onloadend = () => {
            setImageUrl(reader.result);
        };

        reader.readAsDataURL(file);
    };

    const handleClose = (event, reason) => {
        if (reason === "clickaway") {
            return;
        }
        setOpen(false);
    };

    const getSetting = async () => {
        const url = `${process.env.REACT_APP_FIREBASE_BASE_URL}/companies/show`;
        let result = await axios.get(url, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
        });

        setInitialValues(result.data.data.company);
    };

    useEffect(() => {
        getSetting();
    }, []);

    // Formik with Yup for form validation and submission
    const {
        values,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue,
        errors,
        touched,
    } = useFormik({
        enableReinitialize: true,
        initialValues: initialValues,
        validationSchema: SettingValidation,
        validateOnChange: true,
        validateOnBlur: false,
        onSubmit: async (values, action) => {
            setIsSubmitting(true);

            let bodyFormData = new FormData();
            bodyFormData.append("logo", logo);
            bodyFormData.append("appName", values.appName);
            bodyFormData.append("dateFormat", values.dateFormat);
            bodyFormData.append("timeFormat", values.timeFormat);
            bodyFormData.append("language", values.language);
            bodyFormData.append("miniDrawer", values.miniDrawer);

            console.log(bodyFormData);

            // let response = await axios({
            //     method: "put",
            //     url: `${process.env.REACT_APP_FIREBASE_BASE_URL}/companies/update`,
            //     data: bodyFormData,
            //     headers: {
            //         "Content-Type": "multipart/form-data",
            //     },
            // }).then(function (response) {
            //     setInitialValues(response.data.company);
            //     setOpen(true);
            //     setIsSubmitting(false);
            // });
        },
    });

    return (
        <ThemeProvider theme={theme}>
            <Snackbar
                open={open}
                autoHideDuration={6000}
                onClose={() => setOpen(false)}
                message="Data updated successfully"
                action={
                    <React.Fragment>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={handleClose}
                            edge="start"
                        >
                            <ClearIcon />
                        </IconButton>
                    </React.Fragment>
                }
            />

            <Box sx={{ display: "flex" }}>
                {/* This is left sidebar data */}
                <Sidebar title="Settings" />

                <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
                    {/* Don't remove this */}
                    <DrawerHeader />

                    <Grid>
                        <h3>App Settings</h3>
                        <Card>
                            <CardContent>
                                <form onSubmit={handleSubmit}>
                                    <Grid container>
                                        <Grid container xs={4} md={4}>
                                            <Stack>
                                                {imageUrl ? (
                                                    <img
                                                        src={imageUrl}
                                                        alt="Choose"
                                                        height="300"
                                                    />
                                                ) : (
                                                    <img
                                                        src="https://media.istockphoto.com/id/1409329028/vector/no-picture-available-placeholder-thumbnail-icon-illustration-design.jpg?s=612x612&w=0&k=20&c=_zOuJu755g2eEUioiOUdz_mHKJQJn-tDgIAhQzyeKUQ="
                                                        width="90%"
                                                        height="100%"
                                                        alt="No data Found...!"
                                                    />
                                                )}

                                                <label htmlFor="upload-image">
                                                    <input
                                                        id="upload-image"
                                                        hidden
                                                        accept="image/*"
                                                        type="file"
                                                        onChange={
                                                            handleFileUpload
                                                        }
                                                    />
                                                    <Button
                                                        variant="contained"
                                                        component="span"
                                                        style={{ width: "90%" }}
                                                    >
                                                        Upload New Logo
                                                    </Button>
                                                </label>
                                            </Stack>
                                        </Grid>

                                        <Grid
                                            container
                                            xs={8}
                                            md={8}
                                            spacing={2}
                                        >
                                            <Grid item xs={6} md={6}>
                                                <TextField
                                                    fullWidth
                                                    id="app_name"
                                                    label="App Name"
                                                    name="appName"
                                                    autoComplete="appName"
                                                    value={
                                                        values.appName || ""
                                                    }
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    error={
                                                        touched.appName &&
                                                        Boolean(errors.appName)
                                                    }
                                                    helperText={
                                                        touched.appName &&
                                                        errors.appName
                                                    }
                                                />
                                            </Grid>

                                            <Grid item xs={6} md={6}>
                                                <Autocomplete
                                                    disablePortal
                                                    name="dateFormat"
                                                    id="dateFormat"
                                                    options={dateFormats.map(
                                                        (value) => value.format
                                                    )}
                                                    getOptionLabel={(option) =>
                                                        `${option}`
                                                    }
                                                    value={
                                                        values.dateFormat || ""
                                                    }
                                                    onChange={(
                                                        event,
                                                        selectedValue
                                                    ) =>
                                                        setFieldValue(
                                                            "dateFormat",
                                                            selectedValue
                                                        )
                                                    }
                                                    renderInput={(params) => (
                                                        <TextField
                                                            {...params}
                                                            label="Date Format"
                                                            value={
                                                                values.dateFormat ||
                                                                ""
                                                            }
                                                            error={
                                                                touched.dateFormat &&
                                                                Boolean(
                                                                    errors.dateFormat
                                                                )
                                                            }
                                                            helperText={
                                                                touched.dateFormat &&
                                                                errors.dateFormat
                                                            }
                                                        />
                                                    )}
                                                />
                                            </Grid>

                                            <Grid item xs={6} md={6}>
                                                <Autocomplete
                                                    disablePortal
                                                    name="timeFormat"
                                                    id="timeFormat"
                                                    options={timeFormats.map(
                                                        (value) => value.format
                                                    )}
                                                    getOptionLabel={(option) =>
                                                        `${option}`
                                                    }
                                                    value={
                                                        values.timeFormat || ""
                                                    }
                                                    onChange={(
                                                        event,
                                                        selectedValue
                                                    ) =>
                                                        setFieldValue(
                                                            "timeFormat",
                                                            selectedValue
                                                        )
                                                    }
                                                    renderInput={(params) => (
                                                        <TextField
                                                            {...params}
                                                            label="Time Format"
                                                            value={
                                                                values.timeFormat ||
                                                                ""
                                                            }
                                                            error={
                                                                touched.timeFormat &&
                                                                Boolean(
                                                                    errors.timeFormat
                                                                )
                                                            }
                                                            helperText={
                                                                touched.timeFormat &&
                                                                errors.timeFormat
                                                            }
                                                        />
                                                    )}
                                                />
                                            </Grid>

                                            <Grid item xs={6} md={6}>
                                                <Autocomplete
                                                    disablePortal
                                                    name="language"
                                                    id="language"
                                                    options={languages}
                                                    getOptionLabel={(option) =>
                                                        option
                                                            .charAt(0)
                                                            .toUpperCase() +
                                                        option.slice(1)
                                                    }
                                                    value={
                                                        values.language || ""
                                                    }
                                                    onChange={(
                                                        event,
                                                        selectedValue
                                                    ) =>
                                                        setFieldValue(
                                                            "language",
                                                            selectedValue
                                                        )
                                                    }
                                                    renderInput={(params) => (
                                                        <TextField
                                                            {...params}
                                                            label="Language"
                                                            value={
                                                                values.language ||
                                                                ""
                                                            }
                                                            error={
                                                                touched.language &&
                                                                Boolean(
                                                                    errors.language
                                                                )
                                                            }
                                                            helperText={
                                                                touched.language &&
                                                                errors.language
                                                            }
                                                        />
                                                    )}
                                                />
                                            </Grid>

                                            <Grid item xs={6} md={6}>
                                                <Autocomplete
                                                    disablePortal
                                                    name="miniDrawer"
                                                    id="miniDrawer"
                                                    options={choices}
                                                    getOptionLabel={(option) =>
                                                        option
                                                            .charAt(0)
                                                            .toUpperCase() +
                                                        option.slice(1)
                                                    }
                                                    value={
                                                        values.miniDrawer || ""
                                                    }
                                                    onChange={(
                                                        event,
                                                        selectedValue
                                                    ) =>
                                                        setFieldValue(
                                                            "miniDrawer",
                                                            selectedValue
                                                        )
                                                    }
                                                    renderInput={(params) => (
                                                        <TextField
                                                            {...params}
                                                            label="Mini Drawer"
                                                            value={
                                                                values.miniDrawer ||
                                                                ""
                                                            }
                                                            error={
                                                                touched.miniDrawer &&
                                                                Boolean(
                                                                    errors.miniDrawer
                                                                )
                                                            }
                                                            helperText={
                                                                touched.miniDrawer &&
                                                                errors.miniDrawer
                                                            }
                                                        />
                                                    )}
                                                />
                                            </Grid>
                                        </Grid>
                                    </Grid>

                                    <Button
                                        type="submit"
                                        startIcon={
                                            isSubmitting ? (
                                                <CircularProgress
                                                    color="inherit"
                                                    size="1rem"
                                                />
                                            ) : (
                                                <SaveIcon />
                                            )
                                        }
                                        variant="contained"
                                        sx={{ mt: 2 }}
                                        disabled={isSubmitting}
                                        style={{ alignItems: "center" }}
                                    >
                                        {isSubmitting ? "Loading..." : "Save"}
                                    </Button>
                                </form>
                            </CardContent>
                        </Card>
                    </Grid>
                </Box>
            </Box>
        </ThemeProvider>
    );
}
