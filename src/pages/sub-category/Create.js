import React, { useState, useEffect } from "react";

// Node packages
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { useFormik } from "formik";

// Custom components
import Sidebar from "../../components/menu/Sidebar";
import { DrawerHeader } from "../../components/menu/SidebarData";
import { SubCategoryValidation } from "../../schemas/SubCategoryValidation";

// React MUI components
import { ThemeProvider, useTheme } from "@mui/material/styles";
import {
    Autocomplete,
    Button,
    Card,
    CardContent,
    FormControlLabel,
    Grid,
    Snackbar,
    Switch,
    TextField,
    Box,
} from "@mui/material";

// Icons
import { IconButton } from "@mui/material";
import ClearIcon from "@mui/icons-material/Clear";
import SaveIcon from "@mui/icons-material/Save";
import CircularProgress from "@mui/material/CircularProgress";

export function Create() {
    const theme = useTheme();
    const navigate = useNavigate(); // Navigation
    const [open, setOpen] = React.useState(false); // Used for Snackbar
    const [isSubmitting, setIsSubmitting] = useState(false); // Used for showing loader in submit button
    const [categories, setCategories] = useState([]); // Categories state

    // Handle Snackbar close event
    const handleClose = (event, reason) => {
        if (reason === "clickaway") {
            return;
        }
        setOpen(false);
    };

    // Fetch all the categories
    const getCategory = async () => {
        let loadedCategories = [];
        let categories = await axios.get(
            `${process.env.REACT_APP_FIREBASE_BASE_URL}/categories`,
            {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
            }
        );

        for (const key in categories.data.categories) {
            loadedCategories.push({
                id: categories.data.categories[key]._id,
                name: categories.data.categories[key].name,
            });
        }

        setCategories(loadedCategories);
    };

    // Runs when component load
    useEffect(() => {
        getCategory(); // Get all the categories after page loading
    }, []);

    // Formik with Yup for form validation and submission
    const {
        values,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue,
        errors,
        touched,
    } = useFormik({
        enableReinitialize: true,
        initialValues: {
            category_id: "",
            name: "",
            status: false,
        },
        validationSchema: SubCategoryValidation,
        validateOnChange: true,
        validateOnBlur: false,
        onSubmit: async (values, action) => {

            alert('hi');

            setIsSubmitting(true);

            let newStatus = values.status === true ? "active" : "inactive";
            values = {
                ...values,
                status: newStatus,
                categoryId: values.category_id,
            };

            let response = await axios.post(
                `${process.env.REACT_APP_FIREBASE_BASE_URL}/sub-categories/store`,
                values,
                {
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem(
                            "token"
                        )}`,
                    },
                }
            );

            if (response.statusText === "OK") {
                setOpen(true);

                setTimeout(() => {
                    navigate("/sub-category");
                }, 1000);
            }
            setIsSubmitting(false);
        },
    });

    return (
        <ThemeProvider theme={theme}>
            <Snackbar
                open={open}
                autoHideDuration={6000}
                onClose={() => setOpen(false)}
                message="Data saved successfully"
                action={
                    <React.Fragment>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={handleClose}
                            edge="start"
                        >
                            <ClearIcon />
                        </IconButton>
                    </React.Fragment>
                }
            />

            <Box sx={{ display: "flex" }}>
                {/* This is left sidebar data */}
                <Sidebar title="Create Sub Category" />

                <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
                    {/* Don't remove this */}
                    <DrawerHeader />

                    <Grid>
                        <h3>Create Sub-Category</h3>
                        <Card>
                            <CardContent
                                component="form"
                                noValidate
                                onSubmit={handleSubmit}
                            >
                                <h4>Sub-Category Details</h4>
                                <Grid container spacing={2}>
                                    <Grid item xs={12} md={4}>
                                        <Autocomplete
                                            disablePortal
                                            name="category_id"
                                            id="category_id"
                                            options={categories}
                                            getOptionLabel={(option) =>
                                                option.name
                                            }
                                            renderOption={(props, option) => {
                                                return (
                                                    <li
                                                        {...props}
                                                        key={option.id}
                                                    >
                                                        {option.name}
                                                    </li>
                                                );
                                            }}
                                            onChange={(event, value) =>
                                                setFieldValue(
                                                    "category_id",
                                                    value.id
                                                )
                                            }
                                            renderInput={(params) => (
                                                <TextField
                                                    {...params}
                                                    label="Category Name"
                                                    error={
                                                        touched.category_id &&
                                                        Boolean(
                                                            errors.category_id
                                                        )
                                                    }
                                                    helperText={
                                                        touched.category_id &&
                                                        errors.category_id
                                                    }
                                                />
                                            )}
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={4}>
                                        <TextField
                                            sx={{ mt: 0 }}
                                            margin="normal"
                                            fullWidth
                                            id="name"
                                            label="Sub-Category name"
                                            name="name"
                                            value={values.name || ""}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            error={
                                                touched.name &&
                                                Boolean(errors.name)
                                            }
                                            helperText={
                                                touched.name && errors.name
                                            }
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={4}>
                                        <FormControlLabel
                                            sx={{ ml: 0, pl: 0 }}
                                            control={<Switch color="primary" />}
                                            label="Status"
                                            labelPlacement="top"
                                            name="status"
                                            value={values.status}
                                            onChange={handleChange}
                                        />
                                    </Grid>
                                </Grid>
                                <Button
                                    type="submit"
                                    startIcon={
                                        isSubmitting ? (
                                            <CircularProgress
                                                color="inherit"
                                                size="1rem"
                                            />
                                        ) : (
                                            <SaveIcon />
                                        )
                                    }
                                    variant="contained"
                                    sx={{ mt: 1 }}
                                    disabled={isSubmitting}
                                >
                                    {isSubmitting ? "Loading..." : "Save"}
                                </Button>
                            </CardContent>
                        </Card>
                    </Grid>
                </Box>
            </Box>
        </ThemeProvider>
    );
}
