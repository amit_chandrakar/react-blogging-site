import React, { useState, useEffect } from "react";

// Node packages
import { useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import { useFormik } from "formik";

// Custom components
import Sidebar from "../../components/menu/Sidebar";
import { DrawerHeader } from "../../components/menu/SidebarData";
import { SubCategoryValidation } from "../../schemas/SubCategoryValidation";

// React MUI components
import { ThemeProvider, useTheme } from "@mui/material/styles";
import {
    Button,
    Card,
    CardContent,
    FormControlLabel,
    Grid,
    Snackbar,
    Switch,
    TextField,
    Box,
} from "@mui/material";

// Icons
import { IconButton } from "@mui/material";
import ClearIcon from "@mui/icons-material/Clear";
import SaveIcon from "@mui/icons-material/Save";
import CircularProgress from "@mui/material/CircularProgress";

export function Edit() {
    const theme = useTheme();
    const navigate = useNavigate();
    const [open, setOpen] = React.useState(false);
    const subCategory = useParams("id");
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [initialValues, setInitialValues] = useState({}); // Form initial values
    const [categories, setCategories] = useState([]);
    const [selectedCategory, setSelectedCategory] = useState("");
    const [loader, setLoader] = useState(true); // Stores dataTable loader data

    const handleClose = (event, reason) => {
        if (reason === "clickaway") {
            return;
        }
        setOpen(false);
    };

    useEffect(() => {
        getCategory();
        getSubCategory();
    }, []);

    // Fetch all the categories
    const getCategory = async () => {
        let loadedCategories = [];

        try {
            var categories = await axios.get(
                `${process.env.REACT_APP_FIREBASE_BASE_URL}/categories`,
                {
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem(
                            "token"
                        )}`,
                    },
                }
            );
        } catch (error) {
            console.log(error);
        }

        for (const key in categories.data.categories) {
            loadedCategories.push({
                id: categories.data.categories[key].id,
                name: categories.data.categories[key].name,
            });
        }

        setCategories(loadedCategories);
    };

    const getSubCategory = async () => {
        const url = `${process.env.REACT_APP_FIREBASE_BASE_URL}/sub-categories/edit/${subCategory.id}`;

        let result = null;

        try {
            result = await axios.get(url, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
            });
        } catch (error) {
            console.log(error);
        }

        setInitialValues({
            name: result.data.data.subCategory.name,
            category_id: result.data.data.subCategory.categoryId,
            category_name: result.data.data.subCategory.category[0].name,
            status: result.data.data.subCategory.status == "active" ? true : false,
        });

        setSelectedCategory(result.data.data.subCategory.category[0]._id);
    };

    // Formik with Yup for form validation and submission
    const { values, handleBlur, handleChange, handleSubmit, setFieldValue, errors, touched } =
        useFormik({
            enableReinitialize: true,
            initialValues: initialValues,
            validationSchema: SubCategoryValidation,
            validateOnChange: true,
            validateOnBlur: false,
            onSubmit: async (values, action) => {
                setIsSubmitting(true);

                let newStatus = values.status === true ? "active" : "inactive";
                values = { ...values, status: newStatus, categoryId: values.category_id, categoryName: values.category_name };

                let response = await axios.put(
                    `${process.env.REACT_APP_FIREBASE_BASE_URL}/sub-categories/update/${subCategory.id}`,
                    values,
                    {
                        headers: {
                            Authorization: `Bearer ${localStorage.getItem(
                                "token"
                            )}`,
                        },
                    }
                );

                if (response.statusText === "OK") {
                    setOpen(true);

                    setTimeout(() => {
                        navigate("/sub-category");
                    }, 1000);
                }
                setIsSubmitting(false);
            },
        });

    return (
        <ThemeProvider theme={theme}>
            <Snackbar
                open={open}
                autoHideDuration={6000}
                onClose={() => setOpen(false)}
                message="Data saved successfully"
                action={
                    <React.Fragment>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={handleClose}
                            edge="start"
                        >
                            <ClearIcon />
                        </IconButton>
                    </React.Fragment>
                }
            />

            <Box sx={{ display: "flex" }}>
                {/* This is left sidebar data */}
                <Sidebar title="Edit Sub Category" />

                <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
                    {/* Don't remove this */}
                    <DrawerHeader />

                    <Grid>
                        <h3>Edit Category</h3>
                        <Card>
                            <CardContent
                                component="form"
                                noValidate
                                onSubmit={handleSubmit}
                            >
                                <h4>Sub-Category Details</h4>
                                <Grid container spacing={2}>
                                    <Grid item xs={12} md={4}>
                                        <TextField
                                            sx={{ mt: 0 }}
                                            margin="normal"
                                            fullWidth
                                            label="Category Name"
                                            name="category_name"
                                            value={
                                                initialValues.category_name ||
                                                ""
                                            }
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={4}>
                                        <TextField
                                            sx={{ mt: 0 }}
                                            margin="normal"
                                            fullWidth
                                            id="name"
                                            label="Sub-Category Name"
                                            name="name"
                                            value={values.name || ""}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            error={
                                                touched.name &&
                                                Boolean(errors.name)
                                            }
                                            helperText={
                                                touched.name && errors.name
                                            }
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={4}>
                                        <FormControlLabel
                                            sx={{ ml: 0, pl: 0 }}
                                            control={
                                                <Switch
                                                    color="primary"
                                                    checked={
                                                        values.status
                                                            ? true
                                                            : false
                                                    }
                                                />
                                            }
                                            label="Status"
                                            labelPlacement="top"
                                            name="status"
                                            value={values.status || ""}
                                            onChange={handleChange}
                                        />
                                    </Grid>
                                </Grid>
                                <Button
                                    type="submit"
                                    startIcon={
                                        isSubmitting ? (
                                            <CircularProgress
                                                color="inherit"
                                                size="1rem"
                                            />
                                        ) : (
                                            <SaveIcon />
                                        )
                                    }
                                    variant="contained"
                                    sx={{ mt: 1 }}
                                    disabled={isSubmitting}
                                >
                                    {isSubmitting ? "Loading..." : "Save"}
                                </Button>
                            </CardContent>
                        </Card>
                    </Grid>
                </Box>
            </Box>
        </ThemeProvider>
    );
}
