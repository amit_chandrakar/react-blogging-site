import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { ThemeProvider } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Sidebar from "../../components/menu/Sidebar";
import { DrawerHeader } from "../../components/menu/SidebarData";
import { useTheme } from "@mui/material/styles";
import { Badge, Card, CardContent, Grid } from "@mui/material";
import axios from "axios";

export function Show() {
    const theme = useTheme();
    const [category, setCategory] = useState("");
    const [name, setName] = useState("");
    const [status, setStatus] = useState("inactive");
    const subCategory = useParams("id");

    const getSubCategory = async () => {
        const url = `${process.env.REACT_APP_FIREBASE_BASE_URL}/sub-categories/edit/${subCategory.id}`;

        let result = null;

        try {
            result = await axios.get(url, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
            });
        } catch (error) {
            console.log(error);
        }

        setName(result.data.data.subCategory.name);
        setStatus(result.data.data.subCategory.status);
        setCategory(result.data.data.subCategory.category[0].name);
    };

    useEffect(() => {
        getSubCategory();
    }, []);

    return (
        <ThemeProvider theme={theme}>
            <Box sx={{ display: "flex" }}>
                {/* This is left sidebar data */}
                <Sidebar title="Show Sub Category" />

                <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
                    {/* Don't remove this */}
                    <DrawerHeader />

                    <Grid>
                        <h3>Sub-Category Details</h3>
                        <Card>
                            <CardContent>
                                <Grid container spacing={2}>
                                    <Grid item xs={6} md={6}>
                                        Category Name
                                    </Grid>
                                    <Grid item xs={6} md={6}>
                                        {category}
                                    </Grid>
                                    <Grid item xs={6} md={6}>
                                        Sub-Category Name
                                    </Grid>
                                    <Grid item xs={6} md={6}>
                                        {name}
                                    </Grid>
                                    <Grid item xs={6} md={6}>
                                        Status
                                    </Grid>
                                    <Grid item xs={6} md={6}>
                                        <Badge
                                            sx={{ ml: 3 }}
                                            badgeContent={
                                                status.charAt(0).toUpperCase() +
                                                status.slice(1)
                                            }
                                            color={
                                                status === "active"
                                                    ? "primary"
                                                    : "error"
                                            }
                                        />
                                    </Grid>
                                </Grid>
                            </CardContent>
                        </Card>
                    </Grid>
                </Box>
            </Box>
        </ThemeProvider>
    );
}
