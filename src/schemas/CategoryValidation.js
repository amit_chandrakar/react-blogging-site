import * as Yup from "yup";

export const CategoryValidation = Yup.object({
    name: Yup.string().min(2).max(25).required("First Name field is required"),
});