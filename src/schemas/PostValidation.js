import * as Yup from "yup";

export const PostValidation = Yup.object({
    // category: Yup.string().required("This field is required"),
    // sub_category: Yup.string().required("This field is required"),
    title: Yup.string().min(5).required("This field is required"),
    sub_title: Yup.string().min(5).required("This field is required"),
    long_description: Yup.string().min(10).required("This field is required"),
});