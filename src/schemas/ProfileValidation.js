import * as Yup from "yup";

export const ProfileValidation = Yup.object({
    name: Yup.string().required("This field is required"),
    email: Yup.string().required("This field is required"),
    mobile: Yup.number().min(10).required("This field is required"),
    password: Yup.string().required("This field is required"),
    dob: Yup.date().required("This field is required"),
    gender: Yup.string().required("This field is required"),
    address: Yup.string().required("This field is required"),
});
