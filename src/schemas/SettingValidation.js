import * as Yup from "yup";

export const SettingValidation = Yup.object({
    app_name: Yup.string().required("This field is required"),
    date_format: Yup.string().required("This field is required"),
    time_format: Yup.string().required("This field is required"),
    language: Yup.string().required("This field is required"),
    miniDrawer: Yup.string().required("This field is required"),
});