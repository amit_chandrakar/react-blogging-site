import * as Yup from "yup";

export const SignInValidation = Yup.object({
    email: Yup.string().email().required("Email field is required"),
    password: Yup.string().min(6).required("Password field is required")
});