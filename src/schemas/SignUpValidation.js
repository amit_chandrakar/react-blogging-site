import * as Yup from "yup";

export const SignUpValidation = Yup.object({
    firstName: Yup.string().min(2).max(25).required("First Name field is required"),
    lastName: Yup.string().min(2).max(30).required('Last Name field is required'),
    email: Yup.string().email().required("Email field is required"),
    password: Yup.string().min(6).required("Password field is required"),
    confirmPassword: Yup.string()
        .required("Confirm Password field is required")
        .oneOf([Yup.ref("password"), null], "Password must match"),
});