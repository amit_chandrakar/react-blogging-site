import * as Yup from "yup";

export const SubCategoryValidation = Yup.object({
    category_id: Yup.string().required("Category field is required"),
    name: Yup.string().min(2).max(25).required("Sub Category Name field is required"),
});