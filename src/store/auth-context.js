import React, { useState, useEffect } from 'react';
import axios from 'axios';

const AuthContext = React.createContext({
    isLoggedIn: false,
    onLogout: () => { },
    onLogin: (email, password) => { }
});

export const AuthContextProvider = (props) => {
    const [isLoggedIn, setIsLoggedIn] = useState(false);

    useEffect(() => {
        const storedUserLoggedInInformation = localStorage.getItem('isLoggedIn');

        if (storedUserLoggedInInformation === '1') {
            setIsLoggedIn(true);
        }
    }, []);

    const logoutHandler = () => {
        localStorage.clear();
        setIsLoggedIn(false);
    };

    const loginHandler = async (values) => {
        let response = null;

        try {
            response = await axios.post(`${process.env.REACT_APP_FIREBASE_BASE_URL}/auth/login`, values, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
            });
        } catch (error) {
            console.log(error);
        }

        if (response.data.status === "success") {
            localStorage.setItem('isLoggedIn', '1');
            setIsLoggedIn(true);
            // Store the user in localStorage
            localStorage.setItem("user", JSON.stringify(response.data.data.user));
            localStorage.setItem("company", JSON.stringify(response.data.data.company));
            localStorage.setItem("token", response.data.data.token);
            return true;
        } else {
            return false;
        }
    };

    return (
        <AuthContext.Provider
            value={{
                isLoggedIn: isLoggedIn,
                onLogout: logoutHandler,
                onLogin: loginHandler,
            }}
        >
            {props.children}
        </AuthContext.Provider>
    );
};

export default AuthContext;